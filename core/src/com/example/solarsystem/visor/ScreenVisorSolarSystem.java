/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.visor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.example.solarsystem.app.utils.IAppInfo;
import com.example.solarsystem.corpse.core.SolarSystem;
import com.example.solarsystem.screen.AbstractScreen;
import com.example.solarsystem.ui.FocusIntermediate;
import com.example.solarsystem.utils.exceptions.AliasNotStoredException;
import com.example.solarsystem.utils.exceptions.AssetTypeNotStoredException;

public class ScreenVisorSolarSystem extends AbstractScreen {
	
	//private ResourceManager manager;
	//private DefaultInterface defInterface;
	private Scenario scenario;
	private InputMultiplexer input;
	private InterfaceDefault defUi;
	private SolarSystem system;
	private FocusIntermediate focalizableIntermediate;
	
	public ScreenVisorSolarSystem(IAppInfo infoPack, SolarSystem system) {
		super(infoPack);
		this.system = system;
		input = new InputMultiplexer();
	}
	
	public void initScreen() throws AliasNotStoredException, AssetTypeNotStoredException {
		//try {
		scenario = new Scenario(system, infoPack.screenWidth(), infoPack.screenHeight());
		
		defUi = new InterfaceDefault(infoPack, system, system, scenario.getSpecificCamera(), system.getFirstCorpseName());
		input.addProcessor(defUi);
		input.addProcessor(scenario.getInputProcessor());
		focalizableIntermediate = new FocusIntermediate(scenario);
		focalizableIntermediate.addSetteable(defUi);
		//} catch (AliasNotStoredException | AssetTypeNotStoredException e) {
		//	infoPack.changer().showError("Error on loading the graphics of UI.", e);
		//}
	}

	@Override
	public void show() {
		
	}

	@Override
	public void render(float delta) {
		//Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        //Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		scenario.update(delta);
		scenario.render();
		defUi.act();
		defUi.draw();
		
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public InputProcessor getInputProcessor() {
		return input;
	}
	
	public ICorpseFocalizable getCorpseFocalizable() {
		return focalizableIntermediate;
	}

}
