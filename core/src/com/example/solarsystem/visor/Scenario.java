/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.visor;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureAdapter;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.example.solarsystem.camera.AroundCamera;
import com.example.solarsystem.camera.ICamereable;
import com.example.solarsystem.camera.ISpecificCamera;
import com.example.solarsystem.corpse.core.Corpse;
import com.example.solarsystem.corpse.core.SolarSystem;
import com.example.solarsystem.corpse.exceptions.CorpseNotFoundException;
import com.example.solarsystem.corpse.utils.IRealDataObtainable;

public class Scenario implements ICamereable, ICorpseFocalizable {
	
	private ModelBatch batch;
	private ISpecificCamera camera;
	//private ResourceManager manager;
	private Environment environment;
	private SolarSystem system;
	//private GestureProcessorListener processor;
	private GestureDetector gestureDetector;
	private String corpseFocus;
	
	public Scenario(SolarSystem system, float screenWidth, float screenHeight) {
		//this.manager = manager;
		this.system = system;
		//system.assignMovements(CorpseMovement.ELLIPTIC);
		
		batch = new ModelBatch();
		
		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.8f, 0.8f, 0.8f, 1.0f));
		
		camera = new AroundCamera(screenWidth, screenHeight);
		//system.get
		try {
			corpseFocus = system.getFirstCorpseName();
			camera.setObjective(system.getMetaCoordinates(corpseFocus));
		} catch (CorpseNotFoundException e) {
			e.printStackTrace();
		}
		
		gestureDetector = new GestureDetector(new GestureProcessorListener());
		
		ScreenViewport viewPortScreen = new ScreenViewport(camera.getCamera());
		//viewPortScreen.apply();
	}
	
	public IRealDataObtainable getRealAttributesObtainable() {
		return system;
	}
	
	public InputProcessor getInputProcessor() {
		return gestureDetector;
	}
	
	public void update(float delta) {
		system.update(delta);
		camera.update(delta);
	}
	
	public ISpecificCamera getSpecificCamera() {
		return camera;
	}
	
	public void render() {
		//Collection<Renderable> renderables = ;
		//Collection<RenderableProvider> renderables = system.getRenderables();
		//RenderableProvider prov = new R
		//batch.render(itRend, environment);
		batch.begin(camera.getCamera());
		for (Corpse renderable : system.getCorpses()) {
			//batch.draw(renderable, environment);
			batch.render(renderable, environment);
		}
		batch.end();
		//batch.render( itRend, environment);
		//batch.render(itRend, environment);
		//Array<ModelBatch>
		//batch.rend
		//for (Renderable rend : system.getRenderables()) {
		//	batch.re
		//}
	}

	@Override
	public void setCamera(ISpecificCamera camera) {
		this.camera = camera;
	}
	
	private class GestureProcessorListener extends GestureAdapter {

		@Override
		public boolean pan(float x, float y, float deltaX, float deltaY) {
			camera.move(x, y, deltaX, deltaY);
			return true;
		}

		@Override
		public boolean zoom(float initialDistance, float distance) {
			camera.zoom((distance - initialDistance) / 2000);
			return true;
		}

		
	}

	@Override
	public void setFocusOn(String corpseName) throws CorpseNotFoundException {
		camera.setObjective(system.getMetaCoordinates(corpseName));
		corpseFocus = corpseName;
	}

	@Override
	public String getCorpseFocus() {
		return corpseFocus;
	}
}
