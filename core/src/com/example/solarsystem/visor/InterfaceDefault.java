/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.visor;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.example.solarsystem.app.utils.IAppInfo;
import com.example.solarsystem.camera.ISpecificCamera;
import com.example.solarsystem.corpse.exceptions.CorpseNotFoundException;
import com.example.solarsystem.corpse.movement.IVelocitySetteable;
import com.example.solarsystem.corpse.utils.IRealDataObtainable;
import com.example.solarsystem.ui.UIConstants;
import com.example.solarsystem.utils.exceptions.AliasNotStoredException;
import com.example.solarsystem.utils.exceptions.AssetTypeNotStoredException;

public class InterfaceDefault extends Stage implements ICorpseSetteable {
	
	private IAppInfo appInfo;
	private IRealDataObtainable obtainable;
	private IVelocitySetteable veloSetter;
	private ISpecificCamera camera;
	
	private Skin uiSkin;
	
	private Slider slider;
	private TextButton buttonReset;
	private TextButton buttonMenu;
	private TextButton buttonInfo;
	private Container<Label> containerLabel;
	private Label labelCorpse;
	
	private String currentFocus;
	//private ScreenChangeable screenChanger;
	
	public InterfaceDefault(IAppInfo appInfo, IRealDataObtainable obtainable, 
			IVelocitySetteable veloSetter, ISpecificCamera camera, String corpseDefaultName) throws AliasNotStoredException, AssetTypeNotStoredException {
		this.appInfo = appInfo;
		//this.obtainable = obtainable;
		this.veloSetter = veloSetter;
		this.camera = camera;
		this.obtainable = obtainable;
		//try {//UIConstants.SKIN_ALIAS
		uiSkin = appInfo.manager().getSkinByAlias(UIConstants.SKIN_ALIAS);
		//} catch (AliasNotStoredException | AssetTypeNotStoredException e) {
		//	appInfo.changer().showError("Error on loading the graphics of UI.", e);
		//}
		
		int actorHeight = appInfo.screenHeight() / 10;
		int actorWidth = appInfo.screenWidth() / 4;
		//SliderStyle style = new SliderStyle();
		//style.
		slider = new Slider(0, 100, 1, false, uiSkin);
		slider.setWidth(appInfo.screenWidth() / 3 * 2);
		slider.setHeight(actorHeight * 1.2f);
		slider.setPosition(appInfo.screenWidth() / 2 - slider.getWidth() / 2, appInfo.screenHeight() - slider.getHeight());
		//slider.setV
		addActor(slider);
		slider.addListener(new DragSliderListener());
		
		buttonReset = new TextButton(UIConstants.BUTTON_RESET_TEXT, uiSkin);
		buttonReset.setWidth(actorWidth);
		buttonReset.setHeight(actorHeight);
		buttonReset.setPosition(appInfo.screenWidth() / 24f, actorHeight / 3);
		buttonReset.addListener(new ButtonResetCameraListener());
		addActor(buttonReset);
		
		buttonMenu = new TextButton(UIConstants.BUTTON_MENU_TEXT, uiSkin);
		buttonMenu.setWidth(actorWidth);
		buttonMenu.setHeight(actorHeight);
		buttonMenu.setPosition(appInfo.screenWidth() / 8f + actorWidth, actorHeight / 3);
		buttonMenu.addListener(new ButtonToTreeListener());
		addActor(buttonMenu);
		
		buttonInfo = new TextButton(UIConstants.BUTTON_INFO_TEXT, uiSkin);
		buttonInfo.setWidth(actorWidth);
		buttonInfo.setHeight(actorHeight);
		buttonInfo.setPosition(appInfo.screenWidth() / 4.8f + actorWidth * 2, actorHeight / 3);
		buttonInfo.addListener(new ButtonToInfoListener());
		addActor(buttonInfo);
		
		labelCorpse = new Label(corpseDefaultName, uiSkin);
		//labelCorpse.set
		//labelCorpse.setWidth(actorWidth);
		//labelCorpse.setHeight(actorHeight);
		//labelCorpse.setPosition(0, appInfo.screenHeight - actorHeight * 2);
		//labelCorpse.set
		//addActor(labelCorpse);
		
		containerLabel = new Container<Label>(labelCorpse);
		containerLabel.setWidth(labelCorpse.getWidth() + 50);
		containerLabel.setHeight(actorHeight);
		containerLabel.setPosition(0, appInfo.screenHeight()  - actorHeight * 2);
		//Texture text = null;
		//try {
			//text = appInfo.manager().getTextureByAlias("Ako");
		//} catch (AliasNotStoredException | AssetTypeNotStoredException e) {
		//	appInfo.changer().showError("Error on loading the graphics of UI.", e);
		//}
		//Sprite spr = new Sprite(text);
		//SpriteDrawable sprDr = new SpriteDrawable(spr);
		//containerLabel.setBackground(sprDr);
		addActor(containerLabel);
		
		currentFocus = corpseDefaultName;
	}
	
	@Override
	public void dispose() {
		uiSkin.dispose();
		super.dispose();
	}
	
	@Override
	public void setFocusOn(String corpseName) throws CorpseNotFoundException {
		currentFocus = corpseName;
		labelCorpse.setText(corpseName);
	}
	
	private class ButtonToInfoListener extends ClickListener {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			try {
				appInfo.changer().showInfo(obtainable.getRealAttributesCorpse(currentFocus));
				super.clicked(event, x, y);
			} catch (CorpseNotFoundException e) {
				appInfo.changer().showError("Error when obtain the new corpse.", e);
			}
		}
	}
	
	private class ButtonToTreeListener extends ClickListener {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			appInfo.changer().showTree();
			super.clicked(event, x, y);
		}
	}
	
	private class ButtonResetCameraListener extends ClickListener {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			camera.reset();
			super.clicked(event, x, y);
		}
	}
	
	private class DragSliderListener extends DragListener {
		
		//private Slider slider;
		//private VelocitySetteable veloSetter;
		
		//private float startX;
		//private float screenWidth;
		
		public DragSliderListener(/*Slider slider*//*, VelocitySetteable veloSetter*//*, float screenWidth*/) {
			//this.slider = slider;
			//this.veloSetter = veloSetter;
			//this.screenWidth = screenWidth;
			//startX = 0;
			//appInfo.debugLog.debug("Value slider: " + slider.getValue());
		}
		
		@Override
		public void drag(InputEvent event, float x, float y, int pointer) {
			//super.drag(event, x, y, pointer);
			//float displacement = x - startX;
			//slider.getWidth();
			//slider.getMaxValue();
			//appInfo.debugLog().debug("Value slider: " + Math.pow(slider.getValue(), 2));
			
		}
		@Override
		public void dragStart(InputEvent event, float x, float y, int pointer) {
			//super.dragStart(event, x, y, pointer);
			//startX = x;
			//appInfo.debugLog().debug("Value slider: " + Math.pow(slider.getValue(), 2));
			//veloSetter.setVelocity((float)Math.pow(slider.getValue(), 2));
			if (slider.getValue() != 0)
				veloSetter.setVelocity(slider.getValue());
		}
		@Override
		public void dragStop(InputEvent event, float x, float y, int pointer) {
			//super.dragStop(event, x, y, pointer);
			//appInfo.debugLog.debug("Value slider: " + slider.getValue());
			//appInfo.debugLog().debug("Value slider: " + Math.pow(slider.getValue(), 2));
			if (slider.getValue() != 0)
				veloSetter.setVelocity(slider.getValue());
		}
		
	}

	
}
