/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.utils.misc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.badlogic.gdx.files.FileHandle;
import com.example.solarsystem.utils.management.StoreUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class JsonResourcesUtils {
	
	/*public static final String ASSET_PATH;
	public static final String OUTPUT_FILE;
	public static final String FORMATS_FILE;
	//public static final String[] EXCLUDED_DIRS;
	
	static {
		ASSET_PATH = "../android/assets";
		OUTPUT_FILE = ASSET_PATH + "/configs/avaible_resources.json";
		FORMATS_FILE = ASSET_PATH + "/configs/formats_resources.json";
		//EXCLUDED_DIRS = new String[] {"configs"};
	}*/
	
	/*public static void main(String[] args) {
		File textureDir = new File(ASSET_PATH + "/textures");
		for (File f : textureDir.listFiles()) {
			try {
				if (f.getName().contains("Texture")); {
					String[] names = f.getName().split("Texture");
					//System.out.println(f.getName());
					//System.out.println(names[0] + names[1]);
					f.renameTo(new File(names[0] + names[1]));
					System.out.println();
				}
			} catch (ArrayIndexOutOfBoundsException e) {};
		}
	}*/
	public static void writeText(Writer fileToWrite, String text) throws IOException {
		/*System.out.println(fileToWrite.getAbsolutePath());
		if (!fileToWrite.exists())
			fileToWrite.createNewFile();*/
		//System.out.println(text);
		//System.out.println(text);
		try (PrintWriter writer = new PrintWriter(fileToWrite)) {
			writer.print(text);
			//writer.println(text);
			writer.flush();
			//System.out.println("creat");
		}
	}
	public static void writeText(String fileName, String text) throws IOException {
		writeText(new FileWriter(fileName), text);
	}
	public static String getText(Reader readerHand) throws IOException {
		String text = "";
		try (BufferedReader reader = new BufferedReader(readerHand)) {
			String bufferLine = null;
			while ((bufferLine = reader.readLine()) != null) {
				text += bufferLine;
			}
		}
		return text;
	}
	public static String getText(String fileName) throws FileNotFoundException, IOException {
		return getText(new FileReader(fileName));
	}
	//public static String 
	public static Hashtable<String, List<String>> formatList(String jsonFormat) {
		Type listType = new TypeToken<Hashtable<String, List<String>>>(){}.getType();
		return new Gson().fromJson(jsonFormat, listType);
	}
	public static Hashtable<String, Hashtable<String, String>> resourceMap(String jsonMap) {
		Type mapType = new TypeToken<Hashtable<String, Hashtable<String, String>>>(){}.getType();
		return new Gson().fromJson(jsonMap, mapType);
	}
	public static Hashtable<String, Class<?>> resourceTypes(String jsonTypes) throws ClassNotFoundException {
		Type typesType = new TypeToken<Hashtable<String, String>>(){}.getType();
		Hashtable<String, String> mapPlainTypes = new Gson().fromJson(jsonTypes, typesType);
		Hashtable<String, Class<?>> mapTypes = new Hashtable<String, Class<?>>();
		for (Entry<String, String> ent : mapPlainTypes.entrySet()) {
			//List<Class<?>> typeList = new ArrayList<Class<?>>();
			//for (String className : ent.getValue()) {
			//	typeList.add(Class.forName(className));
			//}
			mapTypes.put(ent.getKey(), Class.forName(ent.getValue()));
		}
		return mapTypes;
	}
	public static <T> String getPrettyJson(T object) {
		Gson gsonBuilder = new GsonBuilder().setPrettyPrinting().create();
		//JsonValue json = new Json
		return gsonBuilder.toJson(object);
	}
	public static Hashtable<String, Hashtable<String, String>> resourceAliasesPath(FileHandle assetDir, Hashtable<String, List<String>> formatList) throws IOException {
		//File assetDir = new File(assetPath);
		//if (!assetDir.exists()) {
		//	throw new IOException("Error, the specified path isn't exist");
		//}
		//if (!assetDir.isDirectory()) {
		//	throw new IOException("Error, the dir of the assets must be a directory.");
		//}
		FileHandle[] filesInAssetDir = assetDir.list();
		//System.out.println(formatList);
		Hashtable<String, Hashtable<String, String>> pathMap = new Hashtable<String, Hashtable<String, String>>();
		for (Entry<String, List<String>> ent : formatList.entrySet()) {
			for (FileHandle f : filesInAssetDir) {
				//System.out.println(f.name());
				if (ent.getKey().equals(f.name())) {
					String regexFormats = StoreUtils.getFormatRegularExpression(ent.getValue(), ".");
					Pattern patternFormat = Pattern.compile(regexFormats);
					Hashtable<String, String> resourceDic = new Hashtable<String, String>();
					for (FileHandle handFile : f.list()) {
						String nameFile = handFile.name();
						Matcher match = patternFormat.matcher(nameFile);
						if (match.find()) {
							String pathAlias = ent.getKey() + "/" + nameFile;
							String resourceAlias = nameFile.split("\\.")[0];
							if (resourceDic.containsKey(resourceAlias)) {
								int i = 0;
								String provisionalName = "";
								while (resourceDic.contains(provisionalName = resourceAlias + i)) {
									i++;
								}
								resourceAlias = provisionalName;
							}
							resourceDic.put(resourceAlias, pathAlias);
						}
					}
					pathMap.put(f.name(), resourceDic);
				}
			}
		}
		return pathMap;
	}
	public static Hashtable<String, Hashtable<String, String>> resourceAliasesPath(
			FileHandle assetDir, Hashtable<String, List<String>> formatList, 
			Hashtable<String, HashSet<String>> aliasesToSave) throws IOException {
//		File assetDir = new File(assetPath);
//		if (!assetDir.exists()) {
//			throw new IOException("Error, the specified path isn't exist");
//		}
//		if (!assetDir.isDirectory()) {
//			throw new IOException("Error, the dir of the assets must be a directory.");
//		}
		FileHandle[] filesInAssetDir = assetDir.list();
		Hashtable<String, Hashtable<String, String>> pathMap = new Hashtable<String, Hashtable<String, String>>();
		for (Entry<String, List<String>> ent : formatList.entrySet()) {
			for (FileHandle f : filesInAssetDir) {
				if (ent.getKey().equals(f.name())) {
					HashSet<String> aliasesAllowed = aliasesToSave.get(f.name());
					String regexFormats = StoreUtils.getFormatRegularExpression(ent.getValue(), ".");
					Pattern patternFormat = Pattern.compile(regexFormats);
					Hashtable<String, String> resourceDic = new Hashtable<String, String>();
					for (FileHandle handleFile : f.list()) {
						String nameFile = handleFile.name();
						Matcher match = patternFormat.matcher(nameFile);
						if (match.find()) {
							String pathAlias = ent.getKey() + "/" + nameFile;
							String resourceAlias = nameFile.split("\\.")[0];
							if (resourceDic.containsKey(resourceAlias)) {
								int i = 0;
								String provisionalName = "";
								while (resourceDic.contains(provisionalName = resourceAlias + i)) {
									i++;
								}
								resourceAlias = provisionalName;
							}
							boolean addResource = true;
							if (aliasesAllowed != null)
								if (!aliasesAllowed.contains(resourceAlias))
									addResource = false;
							if (addResource)
								resourceDic.put(resourceAlias, pathAlias);
						}
					}
					pathMap.put(f.name(), resourceDic);
				}
			}
		}
		return pathMap;
	}
	
//	public static void main(String[] args) throws FileNotFoundException, IOException {
//		String jsonFormats = getText(FORMATS_FILE);
//		Type listType = new TypeToken<Hashtable<String, List<String>>>(){}.getType();
//		Hashtable<String, List<String>> formatList = formatList(jsonFormats, listType);
//		Hashtable<String, Hashtable<String, String>> aliases = resourceAliasesPath(ASSET_PATH, formatList);
//		String jsonAliases = getPrettyJson(aliases);
//		writeText(OUTPUT_FILE, jsonAliases);
//	}
	
//	public static void main(String[] args) throws IOException {
//		File assetDir = new File(ASSET_PATH);
//		if (!assetDir.exists()) {
//			throw new IOException("Error, the specified path isn't exist");
//		}
//		if (!assetDir.isDirectory()) {
//			throw new IOException("Error, the dir of the assets must be a directory.");
//		}
//		BufferedReader reader = new BufferedReader(new FileReader(FORMATS_FILE));
//		String bufferLine = null;
//		String jsonFormats = "";
//		while ((bufferLine = reader.readLine()) != null) {
//			jsonFormats += bufferLine;
//		}
//		Type listType = new TypeToken<Hashtable<String, ArrayList<String>>>(){}.getType();
//		Hashtable<String, List<String>> formatList = new Gson().fromJson(jsonFormats, listType);
//		/*for (Entry<String, List<String>> ent : formatList.entrySet()) {
//			List<String> formatListType = ent.getValue();
//			for (String form : formatListType) {
//				System.out.println(form);
//			}
//		}*/
//		reader.close();
//		
//		File[] filesInAssetDir = assetDir.listFiles();
//		/*List<String> stringDirs = new ArrayList<String>();
//		for (File f : assetDir.listFiles()) {
//			stringDirs.add(f.getName());
//		}*/
//		Hashtable<String, Hashtable<String, String>> pathMap = new Hashtable<String, Hashtable<String, String>>();
//		
//		for (Entry<String, List<String>> ent : formatList.entrySet()) {
//			for (File f : filesInAssetDir) {
//				if (ent.getKey().equals(f.getName())) {
//					String regexFormats = StoreUtils.getFormatRegularExpression(ent.getValue(), ".");
//					Pattern patternFormat = Pattern.compile(regexFormats);
//					Hashtable<String, String> resourceDic = new Hashtable<String, String>();
//					for (String nameFile : f.list()) {
//						Matcher match = patternFormat.matcher(nameFile);
//						if (match.find()) {
//							String pathAlias = ent.getKey() + "/" + nameFile;
//							//System.out.println(nameFile);
//							String resourceAlias = nameFile.split("\\.")[0];
//							if (resourceDic.containsKey(resourceAlias)) {
//								int i = 0;
//								String provisionalName = "";
//								while (resourceDic.contains(provisionalName = resourceAlias + i)) {
//									i++;
//								}
//								resourceAlias = provisionalName;
//							}
//							resourceDic.put(resourceAlias, pathAlias);
//						}
//					}
//					pathMap.put(f.getName(), resourceDic);
//				}
//			}
//		}
//		PrintWriter writer = new PrintWriter(new FileOutputStream(OUTPUT_FILE));
//		Gson gsonBuilder = new GsonBuilder().setPrettyPrinting().create();
//		writer.print(gsonBuilder.toJson(pathMap));
//		writer.close();
//		
//		/*BufferedReader readerr = new BufferedReader(new FileReader(OUTPUT_FILE));
//		String bufferLinee = null;
//		String jsonFormatss = "";
//		while ((bufferLinee = readerr.readLine()) != null) {
//			jsonFormatss += bufferLinee;
//		}
//		System.out.println(jsonFormatss);
//		Type listTypee = new TypeToken<Hashtable<String, Hashtable<String, String>>>(){}.getType();
//		Hashtable<String, Hashtable<String, String>> formatListt = new Gson().fromJson(jsonFormatss, listTypee);
//		
//		
//		for (Entry<String, Hashtable<String, String>> ent : formatListt.entrySet()) {
//			System.out.println(ent.getKey());
//			for (Entry<String, String> ent2 : ent.getValue().entrySet()) {
//				System.out.println("\t-> " + ent2.getKey() + " " + ent2.getValue());
//			}
//		}*/
//	}
}
