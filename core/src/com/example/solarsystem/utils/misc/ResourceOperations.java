/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.utils.misc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;

import com.badlogic.gdx.files.FileHandle;


public class ResourceOperations {
	
	private FileHandle assetsPath;
	private FileHandle pathJsonResources;
	private FileHandle pathFormatsIndicators;
	private FileHandle pathResourceTypes;
	
	/**
	 * Directory of resource - format list allowed
	 */
	private Hashtable<String, List<String>> formatList;
	/**
	 * Directory of resource - alias of resource - path of resource
	 * 
	 * This dir can be set at time of reading by aliasTypesToLoad
	 */
	private Hashtable<String, Hashtable<String, String>> aliasesResources;
	/**
	 * Directory of resources - aliases in resource
	 */
	private Hashtable<String, HashSet<String>> aliasTypesToLoad;
	/**
	 * Directory of resource - Types of resources
	 */
	private Hashtable<String, Class<?>> resourceTypes;
	private HashSet<String> aliasesToLoad;
	
	public ResourceOperations(FileHandle assetsPath,
			FileHandle pathJsonResources, FileHandle pathFormatsIndicators,
			FileHandle pathResourceTypes) {
		this.assetsPath = assetsPath;
		this.pathJsonResources = pathJsonResources;
		this.pathFormatsIndicators = pathFormatsIndicators;
		this.pathResourceTypes = pathResourceTypes;
		aliasTypesToLoad = new Hashtable<String, HashSet<String>>();
	}
	
	public void readResourceTypes() throws FileNotFoundException, IOException, ClassNotFoundException {
		String jsonResourceTypes = JsonResourcesUtils.getText(pathResourceTypes.reader());
		resourceTypes = JsonResourcesUtils.resourceTypes(jsonResourceTypes);
	}
	public void addAliasesToLoad(List<String> aliases) {
		aliasesToLoad = new HashSet<String>(aliases);
	}
	public void addAliasesToLoad(String resourceType, List<String> aliases) {
		if (aliasTypesToLoad.containsKey(resourceType)) {
			//HashSet<String> currAliases = aliasTypesToLoad.get(resourceType);
			/*for (String str : aliases) {
				if (currAliases.contains(str)) {
					
				}
			}*/
			aliasTypesToLoad.get(resourceType).addAll(aliases);
		}
		else {
			HashSet<String> hashSet = new HashSet<String>(aliases);
			aliasTypesToLoad.put(resourceType, hashSet);
		}
	}
	
	public void readFormatList() throws FileNotFoundException, IOException {
		String jsonFormats = JsonResourcesUtils.getText(pathFormatsIndicators.reader());
		formatList = JsonResourcesUtils.formatList(jsonFormats);
	}
	
	public void readResourceMap() throws FileNotFoundException, IOException {
		String jsonResources = JsonResourcesUtils.getText(pathJsonResources.reader());
		//System.out.println(jsonResources);
		aliasesResources = JsonResourcesUtils.resourceMap(jsonResources);
	}
	
	public void generateResourceMap() throws IOException {
		aliasesResources = JsonResourcesUtils.resourceAliasesPath(assetsPath, formatList);
	}
	public void generateResourceMapOfAliasesNoType() throws IOException {
		for (String directory : formatList.keySet()) {
			aliasTypesToLoad.put(directory, aliasesToLoad);
			for (String str : aliasesToLoad) {
				System.out.println(directory + " : " + str);
			}
		}
		generateResourceMapOfAliases();
	}
	public void generateResourceMapOfAliases() throws IOException {
		aliasesResources = JsonResourcesUtils.resourceAliasesPath(
				assetsPath, formatList, aliasTypesToLoad);
		for (Entry<String, Hashtable<String, String>> string : aliasesResources.entrySet()) {
			for (Entry<String, String>  ent: string.getValue().entrySet()) {
				System.out.println(string.getKey() + " : " + ent.getKey() + " " + ent.getValue());
			}
		}
	}
	
	public void writeResourceMap() throws IOException {
		String jsonResources = JsonResourcesUtils.getPrettyJson(aliasesResources);
		//pathJsonResources.
		JsonResourcesUtils.writeText(pathJsonResources.writer(false), jsonResources);
	}

	public String getAssetsPath() {
		return assetsPath.path();
	}

	public void setAssetsPath(FileHandle assetsPath) {
		this.assetsPath = assetsPath;
	}

	public String getPathJsonResources() {
		return pathJsonResources.path();
	}

	public void setPathJsonResources(FileHandle pathJsonResources) {
		this.pathJsonResources = pathJsonResources;
	}

	public String getPathFormatsIndicators() {
		return pathFormatsIndicators.path();
	}

	public void setPathFormatsIndicators(FileHandle pathFormatsIndicators) {
		this.pathFormatsIndicators = pathFormatsIndicators;
	}

	public Hashtable<String, List<String>> getFormatList() {
		return formatList;
	}

	public void setFormatList(Hashtable<String, List<String>> formatList) {
		this.formatList = formatList;
	}

	public Hashtable<String, Hashtable<String, String>> getAliasesResources() {
		return aliasesResources;
	}

	public void setAliasesResources(
			Hashtable<String, Hashtable<String, String>> aliasesResources) {
		this.aliasesResources = aliasesResources;
	}

	public String getPathResourceTypes() {
		return pathResourceTypes.path();
	}

	public void setPathResourceTypes(FileHandle pathResourceTypes) {
		this.pathResourceTypes = pathResourceTypes;
	}

	public Hashtable<String, HashSet<String>> getAliasTypesToLoad() {
		return aliasTypesToLoad;
	}

	public Hashtable<String, Class<?>> getResourceTypes() {
		return resourceTypes;
	}

	public void setResourceTypes(Hashtable<String, Class<?>> resourceTypes) {
		this.resourceTypes = resourceTypes;
	}
	
}
