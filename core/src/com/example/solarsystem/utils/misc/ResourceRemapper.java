/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.utils.misc;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.badlogic.gdx.files.FileHandle;

public class ResourceRemapper {
	
	public static final String PATH_ASSETS = "../android/assets";
	public static final String PATH_JSONRESOURCES = PATH_ASSETS + "/" +"configs/avaible_resources.json";
	public static final String PATH_JSONFORMATS = PATH_ASSETS + "/" +"configs/formats_resources.json";
	public static final String PATH_JSONCLASSES = PATH_ASSETS + "/" +"configs/literal_classes.json";
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		FileHandle flAssetPath = new FileHandle(PATH_ASSETS);
		FileHandle flJsonResources = new FileHandle(PATH_JSONRESOURCES);
		FileHandle flJsonFormats = new FileHandle(PATH_JSONFORMATS);
		FileHandle flJsonClasses = new FileHandle(PATH_JSONCLASSES);
		
		ResourceOperations op = new ResourceOperations(flAssetPath, flJsonResources, flJsonFormats, flJsonClasses);
		
		op.readFormatList();
		op.generateResourceMap();
		op.writeResourceMap();
	}
}
