/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.utils.management;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.example.solarsystem.utils.exceptions.AliasNotStoredException;
import com.example.solarsystem.utils.exceptions.AssetTypeNotStoredException;

/**
 * <p>This class manages the {@link ResourceStore} class, have a lot of
 * assets type classes to get the different resources stored in the resource manager.</p>
 * 
 * <p>This class supports the following asset types:
 * <ul>
 * 	<li>{@link com.badlogic.gdx.graphics.Texture}</li>
 * 	<li>{@link com.badlogic.gdx.graphics.Pixmap}</li>
 * 	<li>{@link com.badlogic.gdx.scenes.scene2d.ui.Skin}</li>
 * 	<li>{@link com.badlogic.gdx.graphics.g3d.Model}</li>
 * 	<li>{@link com.badlogic.gdx.graphics.g2d.TextureAtlas}</li>
 * 	<li>{@link com.badlogic.gdx.graphics.g2d.BitmapFont}</li>
 * 	<li>{@link com.badlogic.gdx.audio.Music}</li>
 * 	<li>{@link com.badlogic.gdx.audio.Sound}</li>
 * </ul></p>
 * 
 * <p>If {@link com.badlogic.gdx.assets.AssetManager} update sometime,
 * you can extend this class and add more asset types, it's very
 * flexible class.</p>
 *
 * <p>All of this assets are dictate by the {@link IResourceManager} interface.</p>
 * 
 * 
 * @author Adrián Martínez
 *
 */
public class ResourceLoader implements IResourceManager {
	
	private ResourceStore store;
	
	private static Class<Texture> TYPE_TEXTURE = Texture.class;
	private static Class<Skin> TYPE_SKIN = Skin.class;
	private static Class<Model> TYPE_MODEL = Model.class;
	private static Class<Pixmap> TYPE_PIXMAP = Pixmap.class;
	private static Class<BitmapFont> TYPE_BITMAPFONT = BitmapFont.class;
	private static Class<TextureAtlas> TYPE_TEXTUREATLAS = TextureAtlas.class;
	private static Class<Music> TYPE_MUSIC = Music.class;
	private static Class<Sound> TYPE_SOUND = Sound.class;
	
	public ResourceLoader() {
		store = null;
	}
	
	public void setResourceStore(ResourceStore store) {
		this.store = store;
	}
	
	public <T> T getResourceByAlias(Class<T> type, String alias) 
			throws AliasNotStoredException, AssetTypeNotStoredException {
		if (store == null)
			throw new IllegalStateException("The store needs to be initialized.");
		return store.getResourceByAlias(type, alias);
	}

	@Override
	public Texture getTextureByAlias(String alias)
			throws AliasNotStoredException, AssetTypeNotStoredException {
		return getResourceByAlias(TYPE_TEXTURE, alias);
	}

	@Override
	public Skin getSkinByAlias(String alias) throws AliasNotStoredException,
			AssetTypeNotStoredException {
		return getResourceByAlias(TYPE_SKIN, alias);
	}

	@Override
	public Model getModelByAlias(String alias) throws AliasNotStoredException,
			AssetTypeNotStoredException {
		return getResourceByAlias(TYPE_MODEL, alias);
	}

	@Override
	public Pixmap getPixmapByAlias(String alias)
			throws AliasNotStoredException, AssetTypeNotStoredException {
		return getResourceByAlias(TYPE_PIXMAP, alias);
	}

	@Override
	public BitmapFont getBitmapFontByAlias(String alias)
			throws AliasNotStoredException, AssetTypeNotStoredException {
		return getResourceByAlias(TYPE_BITMAPFONT, alias);
	}

	@Override
	public TextureAtlas getTextureAtlasByAlias(String alias)
			throws AliasNotStoredException, AssetTypeNotStoredException {
		return getResourceByAlias(TYPE_TEXTUREATLAS, alias);
	}

	@Override
	public Music getMusicByAlias(String alias) throws AliasNotStoredException,
			AssetTypeNotStoredException {
		return getResourceByAlias(TYPE_MUSIC, alias);
	}

	@Override
	public Sound getSoundByAlias(String alias) throws AliasNotStoredException,
			AssetTypeNotStoredException {
		return getResourceByAlias(TYPE_SOUND, alias);
	}

	@Override
	public void dispose() {
		store.dispose();
		store = null;
	}

}
