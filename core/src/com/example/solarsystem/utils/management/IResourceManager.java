/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.utils.management;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.example.solarsystem.utils.exceptions.AliasNotStoredException;
import com.example.solarsystem.utils.exceptions.AssetTypeNotStoredException;

/**
 * This interface is used to extract assets to the determined store that
 * implements this interface.
 * 
 * @author Adrián Martínez
 *
 */
public interface IResourceManager {
	public Texture getTextureByAlias(String alias) throws AliasNotStoredException, AssetTypeNotStoredException;
	public Skin getSkinByAlias(String alias) throws AliasNotStoredException, AssetTypeNotStoredException;
	public Model getModelByAlias(String alias) throws AliasNotStoredException, AssetTypeNotStoredException;
	public Pixmap getPixmapByAlias(String alias) throws AliasNotStoredException, AssetTypeNotStoredException;
	public BitmapFont getBitmapFontByAlias(String alias) throws AliasNotStoredException, AssetTypeNotStoredException;
	public TextureAtlas getTextureAtlasByAlias(String alias) throws AliasNotStoredException, AssetTypeNotStoredException;
	public Music getMusicByAlias(String alias) throws AliasNotStoredException, AssetTypeNotStoredException;
	public Sound getSoundByAlias(String alias) throws AliasNotStoredException, AssetTypeNotStoredException;
	public void dispose();
}
