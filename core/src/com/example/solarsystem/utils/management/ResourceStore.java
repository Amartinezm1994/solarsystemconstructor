/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.utils.management;

import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;

import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AssetLoader;
import com.example.solarsystem.utils.exceptions.AliasNotStoredException;
import com.example.solarsystem.utils.exceptions.AssetNotLoadedException;
import com.example.solarsystem.utils.exceptions.AssetNotSupportedException;
import com.example.solarsystem.utils.exceptions.AssetTypeAlreadyExistsException;
import com.example.solarsystem.utils.exceptions.AssetTypeNotStoredException;
import com.example.solarsystem.utils.exceptions.IncorrectPathException;
import com.example.solarsystem.utils.exceptions.UnrecognizedFormatException;

/**
 * <p>ResourceStore manage all the resources and depends by the
 * {@link ResourceLoader} class, this class needs 
 * other manager class to be parameterized and extracted
 * to be an interface or other class.</p>
 * 
 * <p>The class store: the resource, a pattern to check if
 * has a correct resource, the path, the name of resource
 * and an alias to call this resource.</p>
 * 
 * <p>This class can store a multiple resources of different
 * types in the same alias.</p>
 * 
 * <p>For example, the class can store a Skin, a Texture and
 * a Model, this three resources have a different name, path
 * and type, but specifying the same alias, this three resources
 * can be called by this alias, the same alias.</p>
 * 
 * <p>This class fully depends of {@link com.badlogic.gdx.assets.AssetManager}.</p>
 * 
 * @author Adrián Martínez
 */
class ResourceStore implements IInformativeLoadeable {
	private AssetManager manager;

	private Hashtable<Class<?>, FolderPattern> folders;
	private Hashtable<String, Hashtable<Class<?>, String>> resources;
	
	private static ResourceStore INSTANCE;
	
	static {
		INSTANCE = null;
	}
	
	private ResourceStore() {
		manager = new AssetManager();
		folders = new Hashtable<Class<?>, FolderPattern>();
		resources = new Hashtable<String, Hashtable<Class<?>, String>>();
	}
	
	/**
	 * Get instance of the resource store, if the instance hasn't
	 * been created, the static method going to create a new instance
	 * of that.
	 * @return the instance
	 */
	protected static ResourceStore getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ResourceStore();
		}
		return INSTANCE;
	}
	
	/**
	 * Add folder of resources, this folder will be
	 * identified by the type of class.
	 * @param folderClass the class in libgdx to be stored.
	 * @param path the path of the folder.
	 * @param formats a {@link java.util.List} of the formats.
	 * @throws IncorrectPathException If the path is incorrect.
	 * @throws AssetNotSupportedException If the specified type class is not supported by AssetManager.
	 */
	protected <T> void addFolder(Class<T> folderClass, String path, 
			List<String> formats) throws IncorrectPathException, AssetNotSupportedException {
		//if (!manager.) {
		//	throw new AssetNotSupportedException("This type of asset is not supported by asset manager.");
		//}
		if (formats.isEmpty())
			throw new IllegalStateException("Error, format list is empty");
		FolderPattern folderPatt = new FolderPattern(formats, path + "/");
		folders.put(folderClass, folderPatt);
		//ModelLoader<ModelParameters> model;
		//FileHandle file;
		//Model mod;
		//mod.
		//ObjLoader modd;
		//AssetDescriptor<Model> ass;
		//ass.
		//file.
		//model.
		//manager.get
	}
	
	/**
	 * This method adds a new resource specified by a type, path and alias.
	 * @param resourceType The class of the resource.
	 * @param path The path to will contain the file.
	 * @param alias The alias to will be stored.
	 * @throws AssetTypeNotStoredException When the type is not stored by folder.
	 * @throws UnrecognizedFormatException When the specified format in path isn't recognized by the folder type.
	 * @throws IncorrectPathException When the path is incorrect, the path must to be same of the type specified.
	 * @throws AssetTypeAlreadyExistsException When the combination of type and alias already exists.
	 */
	protected <T> void addResource(Class<T> resourceType, String path, String alias) 
			throws AssetTypeNotStoredException, UnrecognizedFormatException, 
			IncorrectPathException, AssetTypeAlreadyExistsException {
		resourceOperations(resourceType, path, alias, null);
	}
	protected <T> void addResourceWithParams(Class<T> resourceType, String path, 
			String alias, AssetLoaderParameters<T> loader) throws AssetTypeAlreadyExistsException, 
			UnrecognizedFormatException, IncorrectPathException, AssetTypeNotStoredException {
		resourceOperations(resourceType, path, alias, loader);
	}
	
	private <T> void resourceOperations(Class<T> type, String path, String alias, 
			AssetLoaderParameters<T> loader) throws AssetTypeAlreadyExistsException, 
			UnrecognizedFormatException, IncorrectPathException, AssetTypeNotStoredException {
		//System.out.println(type.getName());
		/*for (Class<?> typeName : folders.keySet()) {
			System.out.println(typeName.getName());
		}*/
		if (!folders.containsKey(type)) {
			throw new AssetTypeNotStoredException("This asset type is not stored.");
		}
		FolderPattern foldPattern = folders.get(type);
		String resourceName = foldPattern.processPath(path);
		Hashtable<Class<?>, String> aliasResources;
		if (!resources.containsKey(alias)) {
			aliasResources = new Hashtable<Class<?>, String>();
			resources.put(alias, aliasResources);
		} else {
			aliasResources = resources.get(alias);
			if (aliasResources.containsKey(type)) {
				throw new AssetTypeAlreadyExistsException("This asset type already exists with this alias.");
			}
		}
		aliasResources.put(type, resourceName);
		if (loader == null)
			manager.load(foldPattern.getPath() + resourceName, type);
		else
			manager.load(foldPattern.getPath() + resourceName, type, loader);
	}
	
	/**
	 * Get the resource specified by alias.
	 * @param resourceType the asset type to get.
	 * @param alias identify the resource type.
	 * @return the instance of resource of type specified and alias identified.
	 * @throws AliasNotStoredException when the alias doesn't exist.
	 * @throws AssetTypeNotStoredException when the type resource not stored by argumented alias.
	 */
	protected <T> T getResourceByAlias(Class<T> resourceType, String alias) 
			throws AliasNotStoredException, AssetTypeNotStoredException {
		if (!resources.containsKey(alias)) {
			throw new AliasNotStoredException("The specified alias isn't stored");
		}
		Hashtable<Class<?>, String> aliasResources = resources.get(alias);
		if (!aliasResources.containsKey(resourceType)) {
			throw new AssetTypeNotStoredException("This asset type had not stored.");
		}
		String completeAssetPath = folders.get(resourceType).getPath() + aliasResources.get(resourceType);
		return manager.get(completeAssetPath, resourceType);
	}
	
	protected <T> T getResource(Class<T> resourceType, String path) throws AssetNotLoadedException {
		manager.update();
		if (!manager.isLoaded(path, resourceType)) {
			throw new AssetNotLoadedException("Asset not loaded");
		}
		return manager.get(path, resourceType);
	}
	
	protected <T, P extends AssetLoaderParameters<T>> void setLoader(Class<T> type, AssetLoader<T, P> loader) {
		manager.setLoader(type, loader);
	}
	
	protected void dispose() {
		folders = null;
		resources = null;
		manager.dispose();
		INSTANCE = null;
	}
	
	protected IInformativeLoadeable getInformativeLoadeable() {
		return this;
	}
	
	protected void finishToLoadAssetAlias(String alias) {
		for (Entry<Class<?>, String> aliasEntry : resources.get(alias).entrySet()) {
			Class<?> aliasClass = aliasEntry.getKey();
			String path = folders.get(aliasClass).getPath();
			String completeAssetPath = path + aliasEntry.getValue();
			manager.update();
			manager.finishLoadingAsset(completeAssetPath);
		}
	}
	
	protected void finishToLoadAsset(String path) {
		manager.update();
		manager.finishLoadingAsset(path);
	}
	
	protected boolean isAssetLoadedByAlias(String alias) {
		for (Entry<Class<?>, String> aliasEntry : resources.get(alias).entrySet()) {
			Class<?> aliasClass = aliasEntry.getKey();
			String path = folders.get(aliasClass).getPath();
			String completeAssetPath = path + aliasEntry.getValue();
			manager.update();
			if (!manager.isLoaded(completeAssetPath))
				return false;
		}
		return true;
	}

	@Override
	public float getProgress() {
		manager.update();
		return manager.getProgress();
	}

	@Override
	public int getLoadedAssets() {
		manager.update();
		return manager.getLoadedAssets();
	}

	@Override
	public int getPendingAssets() {
		manager.update();
		return manager.getQueuedAssets() + manager.getLoadedAssets();
	}

	@Override
	public boolean isAssetLoaded(String pathAsset) {
		return manager.isLoaded(pathAsset);
	}
}
