/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.utils.management;

import java.util.List;

/**
 * <p>This class is used to centralize methods that do not
 * depend of the class uses.</p>
 * 
 * @author Adrián Martínez
 *
 */
public class StoreUtils {
	
	//public static final 
	
	/*public static void main(String[] args) {
		List<String> formats = new ArrayList<String>();
		formats.add("json");
		formats.add("jpg");
		//String regex = "[^/]*(?!(json|io))";
		//String regex = "[^/]+\\.(json|io)$";
		//String regexFolder = "(?:.*(\\/)(?!.*\\/))";
		//String regexToFile = "sda".
		//Pattern patt = Pattern.compile(regex);
		//System.out.println(regex);
		Pattern patt = Pattern.compile(getFormatRegularExpression(formats, "."));
		Matcher match = patt.matcher("asdasd/asdasd/asdasd.jsonad.json");
		if (match.find()) {
			System.out.println(match.group());
		}
		patt = Pattern.compile(getFolderRegularExpression("/"));
		match = patt.matcher("asdasd/asdasd/asdasd.jsonad.json");
		if (match.find()) {
			System.out.println(match.group());
		}
		/*String[] strs = "asdasd/asdasd/asdad.json".split(regex);
		for (String s : strs) {
			System.out.println(s);
		}
		System.out.println(strs[0]);
		//System.out.println(regex);
		//System.out.println(patt.matcher("asdasd/asdasd/asdad.json").matches());
	}*/
	
	/**
	 * <p>Obtain the regular expression to separate the folder of the
	 * filename.</p>
	 * 
	 * <p>If the folder is: sub/sub/file.ext this regular expression
	 * catch the follow string: sub/sub/.</p>
	 * 
	 * <p>This method can construct the regex expression with different
	 * folder separator ("/" in Unix and "\" in Windows).</p>
	 * 
	 * @param separator the separator used to distinct the folder tree.
	 * @return the regular expression.
	 */
	public static String getFolderRegularExpression(String separator) {
		return "(?:.*(\\" + separator + ")(?!.*\\" + separator +"))";
	}
	
	/**
	 * <p>Constructs a regular expression to, first, get the filename, and second,
	 * check if the filename has the correct format.</p>
	 * 
	 * <p>If the specified path hasn't the correct format, the regular expression
	 * doesn't going to catch any text.</p>
	 * 
	 * @param formats a list of desired validate formats.
	 * @param formatIndicator in the most cases is ".".
	 * @return the regular expression.
	 */
	public static String getFormatRegularExpression(List<String> formats, String formatIndicator) {
		String patternStr = "[^/]+\\" + formatIndicator + "(";
		for (String format : formats) {
			patternStr += format + "|";
		}
		patternStr = patternStr.substring(0, patternStr.lastIndexOf("|"));
		patternStr += ")$";
		return patternStr;
	}
}
