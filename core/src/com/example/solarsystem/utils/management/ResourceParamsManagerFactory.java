/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.utils.management;

import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;

import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.utils.Logger;
import com.example.solarsystem.app.loading.ILoadAskable;
import com.example.solarsystem.utils.exceptions.AssetNotLoadedException;
import com.example.solarsystem.utils.exceptions.AssetNotSupportedException;
import com.example.solarsystem.utils.exceptions.AssetTypeAlreadyExistsException;
import com.example.solarsystem.utils.exceptions.AssetTypeNotStoredException;
import com.example.solarsystem.utils.exceptions.IncorrectPathException;
import com.example.solarsystem.utils.exceptions.UnrecognizedFormatException;

public class ResourceParamsManagerFactory implements ILoadAskable {
	
	/*private static final String LOCALIZATION_ALIAS;
	
	static {
		LOCALIZATION_ALIAS = "loadInfoLocalization";
	}*/
	
	private ResourceParamsManager params;
	private IInformativeLoadeable informer;
	//private String localizationPath;
	//private I18NBundle loadingTexts;
	//private Logger infoLog;
	//private 
	
	public ResourceParamsManagerFactory(Logger infoLog) {
		//this.infoLog = infoLog;
		//this.localizationPath = localizationPath;
		//this.loadingTexts = loadingTexts;
		
		//loadingTexts = null;
		params = new ResourceParamsManager();
		informer = params.getLoadingInformation();
		
		//try {
			//params.addResource(I18NBundle.class, localizationPath, LOCALIZATION_ALIAS);
			//params.waitToLoadAsset(localizationPath);
			//loadingTexts = params.getResource(I18NBundle.class, localizationPath);
		//} catch (ManagerException e) {
		//	e.printStackTrace();
		//	infoLog.info("Loading localization didn't loaded.");
		//}
		
		InternalFileHandleResolver resolver = new InternalFileHandleResolver();
		params.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
	}
	
	/*public void init() {
		
	}*/
	
	public void addFolders(Hashtable<String, Class<?>> resourceTypes, 
			Hashtable<String, List<String>> formatList) throws 
			IncorrectPathException, AssetNotSupportedException {
		/*for (Entry<Class<?>, String> ent : resourceTypes.entrySet()) {
			if (formatList.containsKey(ent.getValue())) {
				params.addResourceFolder(ent.getKey(), 
						ent.getValue(), formatList.get(ent.getValue()));
			}
			
		}*/
		//System.out.println("Afegint folders");
		for (Entry<String, Class<?>> ent : resourceTypes.entrySet()) {
			//System.out.println(ent.getKey() + " " + ent.getValue().getName());
			if (formatList.containsKey(ent.getKey())) {
				//System.out.println("Conté");
				params.addResourceFolder(ent.getValue(), ent.getKey(), formatList.get(ent.getKey()));
			}
		}
	}
	
	public void addResources(Hashtable<String, Hashtable<String, String>> aliasesResources, 
			Hashtable<String, Class<?>> resourceTypes) throws AssetTypeNotStoredException, 
			UnrecognizedFormatException, IncorrectPathException, AssetTypeAlreadyExistsException {
		/*
		 * Por cada directorio añadir un type
		 */
		for (Entry<String, Hashtable<String, String>> ent : aliasesResources.entrySet()) {
			String folder = ent.getKey();
			if (resourceTypes.containsKey(folder)) {
				Class<?> type = resourceTypes.get(folder);
				for (Entry<String, String> forEntry : ent.getValue().entrySet()) {
					String resourcePath = forEntry.getValue();
					String resourceAlias = forEntry.getKey();
					params.addResource(type, resourcePath, resourceAlias);
				}
			}
		}
	}
	
	/*public <T> void addSpecificResource(Class<T> type, String path, String alias) 
			throws AssetTypeNotStoredException, UnrecognizedFormatException, 
			IncorrectPathException, AssetTypeAlreadyExistsException {
		params.addResource(type, path, alias);
	}*/
	
	public boolean isResourceLoaded(String path) {
		return informer.isAssetLoaded(path);
	}
	
	/*public List<String> addResources(Hashtable) {
		
	}*/
	public <T> T getResource(Class<T> type, String path) throws AssetNotLoadedException {
		return params.getResource(type, path);
	}

	@Override
	public int getProgress() {
		return informer.getLoadedAssets();
	}

	@Override
	public int getMaxiumProgress() {
		return informer.getPendingAssets();
	}

	@Override
	public String getMessageProgress() {
		/*if (loadingTexts != null) {
			
			return loadingTexts.format(LoadingConstants.MSG_LOADING_ASSETS_MANAGER, loaded, pending, percentage);
		}*/
		int loaded = informer.getLoadedAssets();
		int pending = informer.getPendingAssets();
		if (pending != 0 && loaded != 0) {
			float percentage = loaded * 100f / pending;
			return "Loaded " + loaded + " of " + pending + ", " + percentage + "%.";
		} else {
			return "Loading...";
		}
	}

	@Override
	public boolean isLoaded() {
		return getProgress() >= getMaxiumProgress();
	}

	public IResourceManager getResourceManager() throws InstantiationException, IllegalAccessException {
		return params.getResourceManager();
	}
	
	//public static void main(String[] args) throws InstantiationException, IllegalAccessException, IncorrectPathException, AssetNotSupportedException, AssetTypeNotStoredException, UnrecognizedFormatException, AssetTypeAlreadyExistsException, AliasNotStoredException {
		//ResourceManager manager = getDefaultResourceManager();
		//manager.getTextureByAlias("danks");
		//AssetManager manager = new AssetManager();
		//Gdx.files.
		//manager.load("textures\\baglogic.jpg", Texture.class);
		//manager.finishLoading();
		//manager.update();
		//manager.get("textures\\baglogic.jpg", Texture.class);
	//}
	
	/*public static ResourceManager getResourceManager(ResourceOperations resOperations) {
		ResourceParamsManager params = new ResourceParamsManager();
		
	}*/
	
	/*public static ResourceManager getDefaultResourceManager() throws InstantiationException, 
			IllegalAccessException, IncorrectPathException, AssetNotSupportedException, AssetTypeNotStoredException, UnrecognizedFormatException, AssetTypeAlreadyExistsException {
		ResourceParamsManager params = new ResourceParamsManager();
		List<String> textureFormats = new ArrayList<String>();
		textureFormats.add("jpg");
		textureFormats.add("png");
		
		params.addResourceFolder(Texture.class, "textures/", textureFormats);
		params.addResource(Texture.class, "textures/BlinkTexture.jpg", "danks");
		InformativeLoadeable loading = params.getLoadingInformation();
		if (loading != null) {
			//while (!loading.isFinished())
			//	System.out.println(loading.getLoadedPercentage());
		}
		return params.getResourceManager();
	}*/
	
}
