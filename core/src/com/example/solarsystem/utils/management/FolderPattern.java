/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.utils.management;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.example.solarsystem.utils.exceptions.IncorrectPathException;
import com.example.solarsystem.utils.exceptions.UnrecognizedFormatException;

/**
 * This class is used to be a extended pattern of the
 * one path, this path only has a certain formats,
 * this formats will determine when the class is
 * instantiated.
 * @author Adrián Martínez
 *
 */
public class FolderPattern {
	
	private Pattern formatPattern;
	private String pathFolder;
	
	private static Pattern RESOURCE_PATTERN;
	public static String PATH_INDICATOR;
	public static String FORMAT_INDICATOR;
	
	static {
		PATH_INDICATOR = "/";//File.separator;
		FORMAT_INDICATOR = ".";
		RESOURCE_PATTERN = Pattern.compile(StoreUtils.getFolderRegularExpression(PATH_INDICATOR));
	}
	
	/**
	 * The folder argumented by String needs to have a path indicator, for
	 * example: "/", this path can to be a root path "./".
	 * @param formats A list of formats to will be accepted by this resource path.
	 * @param pathFolder The folder to will be the resource path.
	 * @throws IncorrectPathException When the path is incorrect.
	 */
	public FolderPattern(List<String> formats, String pathFolder) throws IncorrectPathException {
		Matcher equalizer = RESOURCE_PATTERN.matcher(pathFolder);
		//System.out.println(RESOURCE_PATTERN.pattern());
		if (!equalizer.find())
			throw new IncorrectPathException("Incorrect folder.");
		this.pathFolder = pathFolder;
		formatPattern = Pattern.compile(StoreUtils.getFormatRegularExpression(formats, FORMAT_INDICATOR));
	}
	
	/**
	 * The method do all checks to confirm if the path passed by argument
	 * is the correct resource type format and is in the correct folder.
	 * @param path
	 * @return the filename with format extension.
	 * @throws UnrecognizedFormatException if has incorrect format.
	 * @throws IncorrectPathException if has incorrect path or if is in the incorrect folder.
	 */
	public String processPath(String path) throws UnrecognizedFormatException, IncorrectPathException {
		Matcher comparator = formatPattern.matcher(path);
		if (!comparator.find())
			throw new UnrecognizedFormatException("Error, this format is unrecognized.");
		Matcher pathExtractor = RESOURCE_PATTERN.matcher(path);
		if (!pathExtractor.find()) {
			throw new IncorrectPathException("The path of resource is incorrect.");
		}
		String directoryResource = pathExtractor.group();
		//Path pathResource = Paths.get(directoryResource);
		//Path pathFormats = Paths.get(pathFolder);
		//if (pathResource.normalize().compareTo(pathFormats.normalize()) != 0) {
		//System.out.println(directoryResource + " -- " + pathFolder);
		if (!directoryResource.equals(pathFolder)) {
			throw new IncorrectPathException("The path of the resource is different with the resource associated.");
		}
		return comparator.group();
	}
	
	public String getPath() {
		return pathFolder;
	}
}
