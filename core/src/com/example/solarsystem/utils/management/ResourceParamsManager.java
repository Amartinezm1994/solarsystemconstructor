/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.utils.management;

import java.lang.instrument.IllegalClassFormatException;
import java.util.List;

import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.loaders.AssetLoader;
import com.example.solarsystem.utils.exceptions.AssetNotLoadedException;
import com.example.solarsystem.utils.exceptions.AssetNotSupportedException;
import com.example.solarsystem.utils.exceptions.AssetTypeAlreadyExistsException;
import com.example.solarsystem.utils.exceptions.AssetTypeNotStoredException;
import com.example.solarsystem.utils.exceptions.IncorrectPathException;
import com.example.solarsystem.utils.exceptions.UnrecognizedFormatException;

public class ResourceParamsManager {
	
	private ResourceStore store;
	private Class<? extends ResourceLoader> resourceLoaderType;
	private Class<? extends IResourceManager> resourceManagerType;
	
	public ResourceParamsManager() {
		resourceLoaderType = ResourceLoader.class;
		resourceManagerType = IResourceManager.class;
		store = ResourceStore.getInstance();
	}

	public Class<? extends ResourceLoader> getResourceLoaderType() {
		return resourceLoaderType;
	}
	
	public void waitToLoadAssetAlias(String alias) {
		store.finishToLoadAssetAlias(alias);
	}
	public void waitToLoadAsset(String path) {
		store.finishToLoadAsset(path);
	}
	public boolean isAssetLoadedByAlias(String alias) {
		return store.isAssetLoadedByAlias(alias);
	}

	public void setResourceLoaderType(Class<? extends ResourceLoader> resourceLoaderType) 
			throws IllegalClassFormatException {
		if (!resourceManagerType.isAssignableFrom(resourceLoaderType)) {
			throw new IllegalClassFormatException(
					"The loader type class doesn't implements the "
					+ "specific interface specified in class.");
		}
		this.resourceLoaderType = resourceLoaderType;
	}

	public Class<? extends IResourceManager> getResourceManagerType() {
		return resourceManagerType;
	}

	public void setResourceManagerType(Class<? extends IResourceManager> resourceManagerType) 
			throws IllegalClassFormatException {
		if (!resourceManagerType.isAssignableFrom(resourceLoaderType)) {
			throw new IllegalClassFormatException("The interface specified "
					+ "must be implemented by loader specified in class.");
		}
		this.resourceManagerType = resourceManagerType;
	}
	
	public <T> void addResourceFolder(Class<T> folderType, String pathFolder, 
			List<String> formats) throws IncorrectPathException, AssetNotSupportedException {
		store.addFolder(folderType, pathFolder, formats);
	}
	
	public <T> void addResource(Class<T> resourceType, String path, String alias) 
			throws AssetTypeNotStoredException, UnrecognizedFormatException, 
			IncorrectPathException, AssetTypeAlreadyExistsException {
		store.addResource(resourceType, path, alias);
	}
	public <T> void addResourceWithParams(Class<T> resourceType, String path, String alias, 
			AssetLoaderParameters<T> loader) throws AssetTypeNotStoredException, 
			UnrecognizedFormatException, IncorrectPathException, AssetTypeAlreadyExistsException {
		store.addResourceWithParams(resourceType, path, alias, loader);
	}
	
	public <T> T getResource(Class<T> resourceType, String path) throws AssetNotLoadedException {
		return store.getResource(resourceType, path);
	}
	
	public <T, P extends AssetLoaderParameters<T>> void setLoader(Class<T> type, AssetLoader<T, P> loader) {
		store.setLoader(type, loader);
	}
	
	public IInformativeLoadeable getLoadingInformation() {
		return store.getInformativeLoadeable();
	}
	
	
	
	@SuppressWarnings("unchecked")
	public <resourceManagerType> resourceManagerType getResourceManager() 
			throws InstantiationException, IllegalAccessException {
		ResourceLoader loader = resourceLoaderType.newInstance();
		loader.setResourceStore(store);
		return (resourceManagerType) loader;
	}
	
}
