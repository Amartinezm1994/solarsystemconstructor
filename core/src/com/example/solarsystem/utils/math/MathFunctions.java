/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.utils.math;

import com.badlogic.gdx.math.Vector3;

public class MathFunctions {
	public static double distanceBetweenTwoPoints(Vector3 first, Vector3 second) {
		double pointx = Math.pow(second.x - first.x, 2);
		double pointy = Math.pow(second.y - first.y, 2);
		double pointz = Math.pow(second.z - first.z, 2);
		return Math.abs(Math.sqrt(pointx + pointy + pointz));
	}
	public static double distanceBetweenTwoPointsNoRadius(
			Vector3 first, Vector3 second, double radius) throws LessDistanceException {
		double realDistance = distanceBetweenTwoPoints(first, second) - radius;
		if (realDistance < 0)
			throw new LessDistanceException();
		return realDistance;
	}
	public static double distanceForZoom(
			Vector3 first, Vector3 second, double radius, 
			double minimalDistance) throws LessDistanceException {
		double distance = distanceBetweenTwoPointsNoRadius(first, second, radius);
		if (distance < minimalDistance)
			throw new LessDistanceException();
		return distance;
	}
	public static double distanceForZoom(Vector3 first, Vector3 second, double radius, 
			double minimalDistance, double maxiumDistance) throws LessDistanceException,
			MoreDistanceException {
		double distance = distanceForZoom(first, second, radius, minimalDistance);
		if (distance > maxiumDistance)
			throw new MoreDistanceException();
		return distance;
	}
	public static Vector3 directorVector(Vector3 first, Vector3 second) {
		return new Vector3(second.x - first.x, second.y - first.y, second.z - first.z);
	}
	public static Vector3 sumateRealDistance(Vector3 point, Vector3 directorVector, double distance) {
		Vector3 newPoint = new Vector3(
				(float)(directorVector.x * distance), 
				(float)(directorVector.y * distance), 
				(float)(directorVector.z * distance)
		);
		newPoint.x += point.x;
		newPoint.y += point.y;
		newPoint.z += point.z;
		return newPoint;
	}
	public static Vector3 vectorToDistance(Vector3 directorVector, double distance) {
		double multiplier = distance / (
				Math.sqrt(
						Math.pow(directorVector.x, 2) + 
						Math.pow(directorVector.y, 2) + 
						Math.pow(directorVector.z, 2)));
		return new Vector3(
				(float)(directorVector.x * multiplier), 
				(float)(directorVector.y * multiplier), 
				(float)(directorVector.z * multiplier)
		);
	}
}
