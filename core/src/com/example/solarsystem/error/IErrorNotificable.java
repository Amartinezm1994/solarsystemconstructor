/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.error;

/**
 * Notify an error on some implementation, generally
 * in a screen and this screen can manage the error,
 * in this case, the implementation show the new screen
 * and shows the error.
 * 
 * @author Adrián Martínez Montes
 *
 */
public interface IErrorNotificable {
	/**
	 * Notify an error, add a reason because the implementation
	 * won't do nothing with the message in exception.
	 * @param reason is the desired message to show.
	 * @param e is the exception to show in the log.
	 */
	public void onError(String reason, Exception e);
}
