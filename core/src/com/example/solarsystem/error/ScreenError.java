/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.error;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Logger;
import com.example.solarsystem.screen.IInputProcesable;

/**
 * A generic class to show an error when the application
 * can't continue the execution.
 * 
 * @author Adrián Martínez Montes
 *
 */
public class ScreenError extends ScreenAdapter implements IInputProcesable {
	
	private Logger errorLog;
	
	private BitmapFont wordLoading;
	private Batch batch;
	
	private String textToShow;

	/**
	 * Need an error log to show the exception trace.
	 * 
	 * @param errorLog
	 */
	public ScreenError(Logger errorLog) {
		this.errorLog = errorLog;
	}
	
	/**
	 * Set the reason to show an error.
	 * 
	 * @param error set the message to be shown in the screen.
	 * @param exception	set the exception to be shown the trace in the error log.
	 */
	public void addReason(String error, Exception exception) {
		errorLog.error(error, exception);
		textToShow = error;
	}

	@Override
	public void show() {
		batch = new SpriteBatch();
		wordLoading = new BitmapFont();
	}

	@Override
	public void render(float delta) {
		batch.begin();
		wordLoading.draw(batch, textToShow, 20, 20);
		batch.end();
	}
	
	@Override
	public void dispose() {
		batch.dispose();
		wordLoading.dispose();
	}

	@Override
	public InputProcessor getInputProcessor() {
		return null;
	}

}
