/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.screen;

import com.example.solarsystem.corpse.external.IRealData;
import com.example.solarsystem.error.ScreenError;
import com.example.solarsystem.ui.utils.IInfoSetteable;

public class ScreenChanger implements IScreenChangeable {
	
	private AbstractScreen sVisor, sTree, sInfo;
	private IInfoSetteable infoSetter;
	private ScreenError sError;
	private IScreenSetteable setteable;
	
	public ScreenChanger(AbstractScreen sVisor, AbstractScreen sTree, AbstractScreen sInfo,
			IInfoSetteable infoSetter, ScreenError sError, IScreenSetteable setteable) {
		this.sVisor = sVisor;
		this.sTree = sTree;
		this.sInfo = sInfo;
		this.infoSetter = infoSetter;
		this.sError = sError;
		this.setteable = setteable;
	}

	@Override
	public void showVisor() {
		setteable.setScreen(sVisor, sVisor.getInputProcessor());
	}

	@Override
	public void showTree() {
		setteable.setScreen(sTree, sTree.getInputProcessor());
	}

	@Override
	public void showInfo(IRealData dataToShow) {
		infoSetter.setFocusedCorpse(dataToShow);
		setteable.setScreen(sInfo, sInfo.getInputProcessor());
	}

	@Override
	public void showError(String error, Exception exception) {
		sError.addReason(error, exception);
		setteable.setScreen(sError, sError.getInputProcessor());
	}

}
