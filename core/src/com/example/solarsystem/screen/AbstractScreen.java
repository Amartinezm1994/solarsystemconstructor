/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.screen;

import com.badlogic.gdx.ScreenAdapter;
import com.example.solarsystem.app.utils.IAppInfo;
import com.example.solarsystem.utils.exceptions.AliasNotStoredException;
import com.example.solarsystem.utils.exceptions.AssetTypeNotStoredException;

public abstract class AbstractScreen extends ScreenAdapter implements IInputProcesable {
	protected IAppInfo infoPack;
	
	public AbstractScreen(IAppInfo infoPack) {
		this.infoPack = infoPack;
	}
	
	public abstract void initScreen() throws AliasNotStoredException, AssetTypeNotStoredException;
}
