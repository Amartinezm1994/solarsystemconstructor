/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.app;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Logger;
import com.example.solarsystem.app.loading.ScreenLoading;
import com.example.solarsystem.app.utils.AppInfoPackage;
import com.example.solarsystem.app.utils.BundleInformation;
import com.example.solarsystem.corpse.core.SolarSystem;
import com.example.solarsystem.corpse.external.SolarSystemData;
import com.example.solarsystem.corpse.movement.CorpseMovement;
import com.example.solarsystem.error.IErrorNotificable;
import com.example.solarsystem.error.ScreenError;
import com.example.solarsystem.screen.ScreenChanger;
import com.example.solarsystem.screen.IScreenSetteable;
import com.example.solarsystem.ui.ScreenInterfaceInfo;
import com.example.solarsystem.ui.ScreenInterfaceMenu;
import com.example.solarsystem.ui.UIConstants;
import com.example.solarsystem.ui.utils.NodeGenerator;
import com.example.solarsystem.utils.exceptions.AliasNotStoredException;
import com.example.solarsystem.utils.exceptions.AssetTypeNotStoredException;
import com.example.solarsystem.utils.exceptions.ManagerException;
import com.example.solarsystem.utils.management.IResourceManager;
import com.example.solarsystem.utils.misc.ResourceOperations;
import com.example.solarsystem.visor.ScreenVisorSolarSystem;

/**
 * The main class of Solar System Constructor 3D
 * @author Adrián Martínez Montes
 *
 */
public class SolarSystemConstructor extends ApplicationAdapter {
	
	//private ResourceManager manager;
	private SolarSystemData dataSystem;
	private BundleInformation infoPack;
	//private AppInfoPackage infoApp;
	
	private ScreenChanger screenChanger;
	private ScreenSetter screenSetter;
	private NotifyError notifyError;
	
	private int screenWidth;
	private int screenHeight;
	
	//private  screenMenu;
	//private  screenInfo;
	//private  screenVisor;
	private ScreenError screenError;
	
	private Logger infoLog;
	private Logger errorLog;
	
	private Screen actualScreen;
	
	public SolarSystemConstructor(SolarSystemData dataSystem, BundleInformation infoPack) {
		this.dataSystem = dataSystem;
		this.infoPack = infoPack;
	}
	
	@Override
	public void create () {
		infoLog = new Logger("INFO", Logger.INFO);
		errorLog = new Logger("ERROR", Logger.ERROR);
		Gdx.app.setLogLevel(Logger.DEBUG);
		
		screenError = new ScreenError(errorLog);
		//notifyError = screenError;
		screenSetter = new ScreenSetter();
		
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		
		// TODO
		//FileHandle assetsPath = Gdx.files.absolute(Gdx.files.getLocalStoragePath() + "../android/assets");
		FileHandle assetsPath = Gdx.files.internal("");
		infoLog.info(assetsPath.file().getAbsolutePath());
		//for (FileHandle fl : assetsPath.list()) {
			//System.out.println(fl.file().getAbsolutePath());
		//}
		//System.out.println(Gdx.files.getExternalStoragePath());
		//Gdx.app.getFiles().
		/*for (FileHandle fi : Gdx.files.absolute(Gdx.files.getLocalStoragePath() + "/../android/assets").list()) {
			System.out.println(fi.name());
		}*/
		FileHandle pathJsonResources = Gdx.files.internal(infoPack.getPathJsonResources());
		//System.out.println(pathJsonResources.exists());
		FileHandle pathFormatsIndicators = Gdx.files.internal(infoPack.getPathFormatsIndicator());
		FileHandle pathResourceTypes = Gdx.files.internal(infoPack.getPathResourceTypes());
		
		ResourceOperations resOperations = new ResourceOperations(assetsPath, 
				pathJsonResources, pathFormatsIndicators, pathResourceTypes);
		List<String> aliases = dataSystem.getAliasesList();
		aliases.add(UIConstants.SKIN_ALIAS);
		aliases.add(UIConstants.TEXTURE_BG1);
		resOperations.addAliasesToLoad(aliases);
		//resOperations.addAliasesToLoad(dataSystem., aliases);
		notifyError = new NotifyError();
		ScreenLoading loadingScreen = new ScreenLoading(new Inicializer(), notifyError,
				resOperations, infoLog, screenWidth, screenHeight);
		try {
			/*FileHandle prop = Gdx.files.internal(infoPack.getLocalizationFile());
			System.out.println(infoPack.getLocalizationFile());
			System.out.println(prop.exists());
			System.out.println(prop.extension());
			System.out.println(prop.name());
			//I18NBundle.setSimpleFormatter(false);*/
			/*try {
				I18NBundle bund = I18NBundle.createBundle(prop, Locale.ENGLISH);
			} catch (MissingResourceException e) {
				
			}*/
			//loadingScreen.init(Gdx.files.internal(infoPack.getLocalizationFile()));
			loadingScreen.init(infoPack.getLoadingTextureFile());
			
		} catch (ManagerException e) {
			//errorLog.error("Error has ocurred during loading of resources.", e);
			notifyError.onError("Error has ocurred during loading of resources.", e);
		} catch (IOException e) {
			//errorLog.error("Error has ocurred during reading the config files.", e);
			notifyError.onError("Error has ocurred during reading the config files.", e);
		} catch (ClassNotFoundException e) {
			notifyError.onError("Error in file of types of the app.",e);
		}
		changeScreen(loadingScreen);
		//actualScreen = loadingScreen;
	}

	@Override
	public void render () {
		//Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		float delta = Gdx.graphics.getDeltaTime();
		//if (actualScreen != null)
			actualScreen.render(delta);
	}
	
	//https://github.com/libgdx/libgdx/blob/master/gdx/src/com/badlogic/gdx/Game.java
	public void changeScreen(Screen screen) {
		screen.show();
		actualScreen = screen;
	}
	public void changeInputProcessor(InputProcessor proc) {
		Gdx.input.setInputProcessor(proc);
	}
	public void onError(String reason, Exception e) {
		screenError.addReason(reason, e);
		screenSetter.setScreen(screenError, screenError.getInputProcessor());
		//errorLog.error(reason, e);
		//Gdx.app.exit();
	}
	
	private class ScreenSetter implements IScreenSetteable {

		@Override
		public void setScreen(Screen screen, InputProcessor inpProc) {
			//SolarSystemConstructor.this.actualScreen = screen;
			SolarSystemConstructor.this.changeScreen(screen);
			SolarSystemConstructor.this.changeInputProcessor(inpProc);
		}
		
	}
	
	
	
	private class Inicializer implements IAppInicializable {
		
		@Override
		public void inicialize(IResourceManager manager) {
			Logger debugLog = new Logger("DEBUG", Logger.DEBUG);
			
			AppInfoPackage infoApp = new AppInfoPackage(manager, screenChanger, 
					/*new ModelBatch(),*/ new SpriteBatch(),
					infoLog, debugLog, errorLog,
					screenWidth, screenHeight);
			//infoApp = 
			//infoApp.setManager(manager);
			//infoApp.setScreenChanger(screenChanger);
			
			Hashtable<String, List<String>> metadataAttr = null;
			try {
				metadataAttr = dataSystem.generateMetadataAttributes();
			} catch (Exception e1) {
				screenChanger.showError("Error on generating the metadata of attributes.", e1);
			}
			try {
				
				SolarSystem system = new SolarSystem(dataSystem, manager);
				system.assignMovements(CorpseMovement.ELLIPTIC);
				
				
				ScreenInterfaceInfo screenInfo = new ScreenInterfaceInfo(infoApp, metadataAttr);
				screenInfo.initScreen();
				ScreenVisorSolarSystem screenVisor = new ScreenVisorSolarSystem(infoApp, system);
				screenVisor.initScreen();
				ScreenInterfaceMenu screenMenu = new ScreenInterfaceMenu(infoApp, screenVisor.getCorpseFocalizable(), 
						new NodeGenerator(dataSystem, "buit"), dataSystem.getCorpseNameList());
				
				screenChanger = new ScreenChanger(screenVisor, screenMenu, screenInfo, 
						screenInfo, screenError, screenSetter);
				infoApp.setScreenChanger(screenChanger);
				
				
				screenMenu.initScreen();
				screenChanger.showVisor();
			} catch (AliasNotStoredException | AssetTypeNotStoredException e) {
				screenChanger.showError("Error on loading the graphics of UI.", e);
			}
			
		}
		
	}
	private class NotifyError implements IErrorNotificable {

		@Override
		public void onError(String reason, Exception e) {
			SolarSystemConstructor.this.onError("App didn't inicialized, reason: " + reason, e);
		}
		
	}
}
