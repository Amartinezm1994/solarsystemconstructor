/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.app.utils;

import java.util.Hashtable;
import java.util.Locale;

/**
 * This class contains the properties of the
 * application when it launches.
 * 
 * @author Adrián Martínez Montes
 *
 */
public class BundleInformation {
	
	private static final String LOCALIZATION_EXTENSION = ".properties";
	
	@Deprecated
	private String assetsPath;
	private String pathJsonResources;
	private String pathFormatsIndicator;
	private String pathResourceTypes;
	private Hashtable<String, String> aliasTypesToLoad;
	private Locale locale;
	private String localizationFile;
	private String loadingTextureFile;
	
	public BundleInformation() {
	}
	
	@Deprecated
	public String getAssetsPath() {
		return assetsPath;
	}
	@Deprecated
	public void setAssetsPath(String assetsPath) {
		this.assetsPath = assetsPath;
	}

	public String getPathJsonResources() {
		return pathJsonResources;
	}

	public void setPathJsonResources(String pathJsonResources) {
		this.pathJsonResources = pathJsonResources;
	}

	public String getPathFormatsIndicator() {
		return pathFormatsIndicator;
	}

	public void setPathFormatsIndicator(String pathFormatsIndicator) {
		this.pathFormatsIndicator = pathFormatsIndicator;
	}

	public Hashtable<String, String> getAliasTypesToLoad() {
		return aliasTypesToLoad;
	}

	public void setAliasTypesToLoad(Hashtable<String, String> aliasTypesToLoad) {
		this.aliasTypesToLoad = aliasTypesToLoad;
	}
	public boolean isAliasesToLoad() {
		return aliasTypesToLoad != null;
	}

	public String getLocalizationFile() {
		return localizationFile + LOCALIZATION_EXTENSION;
	}

	public void setLocalizationFile(String localizationFile) {
		if (localizationFile.contains(LOCALIZATION_EXTENSION)) {
			localizationFile = localizationFile.split(
					LOCALIZATION_EXTENSION.replace(".", "\\."))[0];
		}
		this.localizationFile = localizationFile;
	}

	public String getLoadingTextureFile() {
		return loadingTextureFile;
	}

	public void setLoadingTextureFile(String loadingTextureFile) {
		this.loadingTextureFile = loadingTextureFile;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		if (locale == null) {
			this.locale = Locale.getDefault();
		} else {
			this.locale = locale;
		}
	}

	public String getPathResourceTypes() {
		return pathResourceTypes;
	}

	public void setPathResourceTypes(String pathResourceTypes) {
		this.pathResourceTypes = pathResourceTypes;
	}
}
