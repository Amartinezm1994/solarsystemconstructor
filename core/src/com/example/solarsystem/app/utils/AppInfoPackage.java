/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.app.utils;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Logger;
import com.example.solarsystem.screen.IScreenChangeable;
import com.example.solarsystem.utils.management.IResourceManager;

/**
 * This class contains the application information
 * and resources for removal. See {@link IAppInfo}.
 * 
 * @author Adrián Martínez Montes
 *
 */
public class AppInfoPackage implements IAppInfo {
	
	private IResourceManager manager;
	private IScreenChangeable screenChanger;
	private SpriteBatch spriteBatch;
	private Logger infoLog;
	private Logger debugLog;
	private Logger errorLog;
	private int screenWidth;
	private int screenHeight;
	
	public AppInfoPackage(IResourceManager manager, IScreenChangeable screenChanger, 
			SpriteBatch spriteBatch, Logger infoLog, Logger debugLog, 
			Logger errorLog, int screenWidth, int screenHeight) {
		this.manager = manager;
		this.screenChanger = screenChanger;
		this.infoLog = infoLog;
		this.debugLog = debugLog;
		this.errorLog = errorLog;
		this.screenHeight = screenHeight;
		this.screenWidth = screenWidth;
		this.spriteBatch = spriteBatch;
	}

	@Override
	public int screenWidth() {
		return screenWidth;
	}

	@Override
	public int screenHeight() {
		return screenHeight;
	}

	@Override
	public IResourceManager manager() {
		return manager;
	}

	@Override
	public IScreenChangeable changer() {
		return screenChanger;
	}

	@Override
	public Logger infoLog() {
		return infoLog;
	}

	@Override
	public Logger debugLog() {
		return debugLog;
	}

	@Override
	public Logger errorLog() {
		return errorLog;
	}
	
	@Override
	public SpriteBatch spriteBatch() {
		return spriteBatch;
	}

	public void setManager(IResourceManager manager) {
		this.manager = manager;
	}

	public void setScreenChanger(IScreenChangeable screenChanger) {
		this.screenChanger = screenChanger;
	}

	public void setInfoLog(Logger infoLog) {
		this.infoLog = infoLog;
	}

	public void setDebugLog(Logger debugLog) {
		this.debugLog = debugLog;
	}

	public void setErrorLog(Logger errorLog) {
		this.errorLog = errorLog;
	}

	public void setScreenWidth(int screenWidth) {
		this.screenWidth = screenWidth;
	}

	public void setScreenHeight(int screenHeight) {
		this.screenHeight = screenHeight;
	}

	public void setSpriteBatch(SpriteBatch spriteBatch) {
		this.spriteBatch = spriteBatch;
	}

	
}
