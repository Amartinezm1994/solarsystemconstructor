/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.app.utils;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Logger;
import com.example.solarsystem.screen.IScreenChangeable;
import com.example.solarsystem.utils.management.IResourceManager;

/**
 * The interface provided to hide the implementation
 * because of should be modified by an abstract inferior
 * level.
 * 
 * @author Adrián Martínez Montes
 *
 */
public interface IAppInfo {
	public int screenWidth();
	public int screenHeight();
	public IResourceManager manager();
	public IScreenChangeable changer();
	public Logger infoLog();
	public Logger debugLog();
	public Logger errorLog();
	public SpriteBatch spriteBatch();
	//public ModelBatch modelBatch();
}
