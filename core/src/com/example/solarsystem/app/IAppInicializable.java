/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.app;

import com.example.solarsystem.utils.management.IResourceManager;

/**
 * This interface aims to initialize the application when all necessary
 * resources has been loaded in the memory.
 * 
 * @author Adrián Martínez Montes
 *
 */
public interface IAppInicializable {
	
	/**
	 * Initialize the application, build the screens, configure the application
	 * to be initiated. The application need a one manager.
	 * @param manager is the interface provider of resources.
	 */
	public void inicialize(IResourceManager manager);
}
