/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.app.loading;
/**
 * This class notices the superior abstract
 * level about the progress of some loadeable
 * class.
 * 
 * @author Adrián Martínez Montes
 *
 */
public interface ILoadAskable {
	
	/**
	 * Get the current progress of the implementation.
	 * @return the current progress.
	 */
	public int getProgress();
	
	/**
	 * Get the maximum progress of the implementation.
	 * @return the maximum progress.
	 */
	public int getMaxiumProgress();
	
	/**
	 * Get the current message of the loadeable implementation.
	 * @return the current loading message.
	 */
	public String getMessageProgress();
	
	/**
	 * 
	 * @return true if the implementation is loaded.
	 */
	public boolean isLoaded();
}
