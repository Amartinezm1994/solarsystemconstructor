/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.app.loading;

import java.io.IOException;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Logger;
import com.example.solarsystem.app.IAppInicializable;
import com.example.solarsystem.error.IErrorNotificable;
import com.example.solarsystem.utils.exceptions.ManagerException;
import com.example.solarsystem.utils.management.ResourceParamsManagerFactory;
import com.example.solarsystem.utils.misc.ResourceOperations;

/**
 * This screen is used for load the all resources indicated in
 * {@link ResourceOperations} and calls to {@link ResourceParamsManagerFactory},
 * in fact, the screen calls to loadeable interface to show
 * the progress and draw it.
 * 
 * @author Adrián Martínez Montes
 *
 */
public class ScreenLoading extends ScreenAdapter {
	
	private IAppInicializable inicializable;
	private IErrorNotificable notifyError;
	
	private Texture backGround;
	private ResourceOperations resOperations;
	private ResourceParamsManagerFactory factory;
	private ILoadAskable askable;
	private SpriteBatch twoDBatch;
	private Logger infoLog;
	
	private int screenWidth;
	private int screenHeight;
	
	private int lastProgress;
	
	private BitmapFont wordLoading;
	private ShapeRenderer shapeRender;
	
	private String imagePath;
	
	public ScreenLoading(IAppInicializable inicializable, IErrorNotificable notifyError/*, BundleInformation infoPack*/,
			ResourceOperations resOperations, Logger infoLog, int screenWidth, int screenHeight) {
		this.inicializable = inicializable;
		this.notifyError = notifyError;
		this.resOperations = resOperations;
		this.infoLog = infoLog;
		this.screenHeight = screenHeight;
		this.screenWidth = screenWidth;
		lastProgress = 0;
	}
	
	/**
	 * Starts the screen to prepare for the execution. 
	 * @param imagePath the path of image to show when loading the resources.
	 * @throws ManagerException throws when an error has been ocurred, the reason is specified in the exception message.
	 * @throws IOException throws when has been ocurred an error when reading the json files.
	 * @throws ClassNotFoundException throws when the class specified in the json files is incorrect.
	 */
	public void init(String imagePath) throws ManagerException, IOException, ClassNotFoundException {
		this.imagePath = imagePath;
		
		resOperations.readFormatList();
		
		resOperations.readResourceMap();
		resOperations.generateResourceMapOfAliasesNoType();
		resOperations.readResourceTypes();
		
		
		factory = new ResourceParamsManagerFactory(infoLog);
		
		factory.addFolders(resOperations.getResourceTypes(), resOperations.getFormatList());
		factory.addResources(resOperations.getAliasesResources(), resOperations.getResourceTypes());
		askable = factory;
	}

	@Override
	public void show() {
		backGround = new Texture(imagePath);
		wordLoading = new BitmapFont();
		shapeRender = new ShapeRenderer();
		twoDBatch = new SpriteBatch();
	}

	@Override
	public void render(float delta) {
		
		float widthProgressBar = (float) ((screenWidth / (askable.getMaxiumProgress() + 0.0001)) * askable.getProgress());
		twoDBatch.begin();
		twoDBatch.draw(backGround, 0, 0, screenWidth, screenHeight);
		wordLoading.draw(twoDBatch, askable.getMessageProgress() , 15, 30);
		twoDBatch.end();
		
		shapeRender.begin(ShapeType.Filled);
		shapeRender.setColor(Color.GRAY);
		shapeRender.rect(0, 5, widthProgressBar, 10);
		shapeRender.end();
		if (lastProgress < askable.getProgress()) {
			lastProgress = askable.getProgress();
			infoLog.info("Loading " +  askable.getProgress() + " of " + askable.getMaxiumProgress());
		}
		if (askable.getProgress() >= askable.getMaxiumProgress()) {
			try {
				inicializable.inicialize(factory.getResourceManager());
			} catch (InstantiationException | IllegalAccessException e) {
				notifyError.onError(e.getMessage(), e);
			}
		}
	}

	@Override
	public void dispose() {
		wordLoading.dispose();
		backGround.dispose();
		shapeRender.dispose();
		twoDBatch.dispose();
	}

}
