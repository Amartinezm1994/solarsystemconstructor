/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.corpse.external;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

//<<<<<<< HEAD
public class RealAttributes implements IRealData, Serializable {
//=======
/**
 * This class contains the needed properties of a corpse
 * for the application can generate the solar system.
 * 
 * @author Adrián Martínez Montes
 *
 */
//public class RealAttributes implements IRealData {
//>>>>>>> d4e36c12ad4fc9d0b53ea54c4be4ada3aa5c31fe
	
	private int order;
	private String resourceAlias;
	private boolean isExistingResource;
	
	
	@AttributeProperties(descriptiveName="Corpse name")
	private String name;
	
	@AttributeProperties(descriptiveName="Corpse center name")
	private String center;
	
	@AttributeProperties(measure="m", descriptiveName="Radius")
	private double radius;
	
	@AttributeProperties(measure="kg/m3", descriptiveName="Density")
	private double density;
	
	@AttributeProperties(measure="kg", descriptiveName="Mass")
	private double mass;
	
	@AttributeProperties(measure="km3", descriptiveName="Volume")
	private double volume;
	
	@AttributeProperties(measure="degrees", descriptiveName="Inclination")
	private double inclination;
	
	@AttributeProperties(measure="km", descriptiveName="Aphelion")
	private double aphelion;
	
	@AttributeProperties(measure="km", descriptiveName="Radius of orbit")
	private double orbitRadius;
	
	@AttributeProperties(measure="n", descriptiveName="Excentricity of orbit")
	private double excentricity;
	
	@AttributeProperties(measure="s", descriptiveName="Period of orbit")
	private double orbitalPeriod;
	
	@AttributeProperties(measure="s", descriptiveName="Sideral period")
	private double sideralPeriod;
	
	@AttributeProperties(measure="bool", descriptiveName="Is horary rotation?")
	private boolean horaryRotation;
	
	@AttributeProperties(measure="perc", descriptiveName="Orbit moment")
	private double orbitMoment;
	
	@AttributeProperties(descriptiveName="Type of corpse")
	private CorpseType corpseType;
	
	
	public RealAttributes() {
		isExistingResource = false;
	}
	
	public Hashtable<String, List<String>> getHashtableMetadata() throws Exception {
		Hashtable<String, List<String>> metadata = new Hashtable<String, List<String>>();
		Class<? extends Annotation> annotationType = AttributeProperties.class;
		for (Field fl : RealAttributes.class.getDeclaredFields()) {
			for (Annotation an : fl.getAnnotations()) {
				if (fl.isAnnotationPresent(annotationType)) {
					AttributeProperties prop = (AttributeProperties)an;
					
					List<String> metaValues = new ArrayList<String>();
					metaValues.add(prop.measure());
					metaValues.add(prop.descriptiveName());
		
					metadata.put(fl.getName(), metaValues);
				}
			}
		}
		return metadata;
	}
	

	@Override
	public Hashtable<String, String> getOrderedAttributes() throws IllegalArgumentException, IllegalAccessException {
		Hashtable<String, String> listAttributes = new Hashtable<String, String>();
		Class<? extends Annotation> annotationType = AttributeProperties.class;
		for (Field fl : RealAttributes.class.getDeclaredFields()) {
			if (fl.isAnnotationPresent(annotationType)) {
				listAttributes.put(fl.getName(), fl.get(this).toString());
			}
		}
		return listAttributes;
	}
	

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Get name of corpse which orbiting this corpse.
	 * If is the center this corpse, return null.
	 * @return name of corpse which orbiting this corpse.
	 */
	@Override
	public String getCenter() {
		return center;
	}

	public void setCenter(String center) {
		this.center = center;
	}

	@Override
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	@Override
	public double getDensity() {
		return density;
	}

	public void setDensity(double density) {
		this.density = density;
	}

	@Override
	public double getMass() {
		return mass;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}

	@Override
	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	@Override
	public double getInclination() {
		return inclination;
	}

	public void setInclination(double inclination) {
		this.inclination = inclination;
	}

	@Override
	public double getAphelion() {
		return aphelion;
	}

	public void setAphelion(double aphelion) {
		this.aphelion = aphelion;
	}
	
	@Override
	public double getOrbitRadius() {
		return orbitRadius;
	}

	public void setOrbitRadius(double orbitRadius) {
		this.orbitRadius = orbitRadius;
	}
	
	@Override
	public double getExcentricity() {
		return excentricity;
	}

	public void setExcentricity(double excentricity) {
		this.excentricity = excentricity;
	}

	@Override
	public double getOrbitalPeriod() {
		return orbitalPeriod;
	}

	public void setOrbitalPeriod(double orbitalPeriod) {
		this.orbitalPeriod = orbitalPeriod;
	}

	@Override
	public double getSideralPeriod() {
		return sideralPeriod;
	}

	public void setSideralPeriod(double sideralPeriod) {
		this.sideralPeriod = sideralPeriod;
	}
	
	@Override
	public boolean isHoraryRotation() {
		return horaryRotation;
	}

	public void setHoraryRotation(boolean horaryRotation) {
		this.horaryRotation = horaryRotation;
	}
	
	@Override
	public double getOrbitMoment() {
		return orbitMoment;
	}

	public void setOrbitMoment(double orbitMoment) {
		this.orbitMoment = orbitMoment;
	}
	
	@Override
	public CorpseType getCorpseType() {
		return corpseType;
	}
	/*public String getCorpseType() {
		return corpseType.toString();
	}*/

	public void setCorpseType(CorpseType corpseType) {
		this.corpseType = corpseType;
	}

	public String getResourceAlias() {
		return resourceAlias;
	}

	public void setResourceAlias(String resourceAlias) {
		this.resourceAlias = resourceAlias;
	}

	public boolean isExistingResource() {
		return isExistingResource;
	}

	public void setExistingResource(boolean isExistingResource) {
		this.isExistingResource = isExistingResource;
	}

	
}
