/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.corpse.external;

import java.util.Hashtable;

/**
 * 
 * 
 * @author Adrián Martínez Montes
 *
 */
public interface IRealData {
	public String getName();
	public String getCenter();
	public double getRadius();
	public double getDensity();
	public double getMass();
	public double getVolume();
	public double getInclination();
	public double getAphelion();
	public double getOrbitRadius();
	public double getExcentricity();
	public double getOrbitalPeriod();
	public double getSideralPeriod();
	public boolean isHoraryRotation();
	public double getOrbitMoment();
	public CorpseType getCorpseType();
	/**
	 * Every attribute has a value, but this value has
	 * a measure unit, this measure unit has been
	 * extracted previously and contains a {@link Hashtable}
	 * with the name of the variable and the descriptive name
	 * with the measure name. This method provide a new
	 * {@link Hashtable} with the variable name and value,
	 * the provider of this metadata can reference the data
	 * with the correct value through the variable name, this
	 * always is the key of {@link Hashtable}.
	 * 
	 * @return a hashtable with the variable name-value.
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public Hashtable<String, String> getOrderedAttributes() throws IllegalArgumentException, IllegalAccessException ;
}
