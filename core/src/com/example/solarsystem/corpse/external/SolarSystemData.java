/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.corpse.external;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;

/**
 * This class groups the different corpse data
 * and manage it through the different methods.
 * 
 * @author Adrián Martínez Montes
 *
 */
public class SolarSystemData implements Serializable {
	
	/**
	 * key: corpse name
	 * value: corpse data 
	 */
	private HashMap<String, RealAttributes> corpses;
	
	public SolarSystemData() {
		corpses = new HashMap<String, RealAttributes>();
	}
	
	/**
	 * Add an instance of {@link RealAttributes} in the
	 * corpse list.
	 * @param corpse a new instance of {@link RealAttributes}.
	 * @return true if the corpse previously exists.
	 */
	public boolean addCorpse(RealAttributes corpse) {
		if (corpses.put(corpse.getName(), corpse) == null)
			return false;
		return true;
	}
	/**
	 * Remove a corpse of the corpse list through corpse name.
	 * @param corpseName the name of corpse desired to be deleted.
	 * @return true if the deleted corpse exists previously.
	 */
	public boolean removeCorpse(String corpseName) {
		if (corpses.remove(corpseName) == null)
			return false;
		return true;
	}
	
	/**
	 * Get the specified {@link RealAttributes} by corpse name.
	 * @param corpseName the corpse name.
	 * @return the {@link RealAttributes} referenced by the corpse name
	 * or null if the specified corpse name doesn't exist.
	 */
	public RealAttributes getCorpse(String corpseName) {
		return corpses.get(corpseName);
	}
	
	/**
	 * Get a {@link Collection} of {@link RealAttributes} stored in
	 * the instance of class.
	 * @return the {@link Collection} of {@link RealAttributes}.
	 */
	public Collection<RealAttributes> getCorpseCollection() {
		return corpses.values();
	}
	
	/**
	 * The list of aliases stored in every {@link RealAttributes}.
	 * 
	 * @return a {@link List} of {@link RealAttributes}.
	 */
	public List<String> getAliasesList() {
		List<String> aliasList = new ArrayList<String>();
		for (Entry<String, RealAttributes> ent : corpses.entrySet()) {
			aliasList.add(ent.getValue().getResourceAlias());
		}
		return aliasList;
	}
	
	/**
	 * A {@link List} of corpse names.
	 * 
	 * @return A {@link List} of corpse names.
	 */
	public List<String> getCorpseNameList() {
		return new ArrayList<String>(corpses.keySet());
	}
	
	/**
	 * Generate and returns the metadata of the {@link RealAttributes},
	 * this metadata contains the next data:
	 * <ul>
	 * 	<li>variable name</li>
	 * 	<li>
	 * 		<ul>
	 * 			<li>descriptive name</li>
	 * 			<li>measure unit</li>
	 * 		</ul>
	 * 	</li>
	 * </ul>
	 * @return a {@link Hashtable} of metadata.
	 * @throws Exception the message exception indicates the reason.
	 */
	public Hashtable<String, List<String>> generateMetadataAttributes() throws Exception {
		return corpses.values().iterator().next().getHashtableMetadata();
	}
	
	
}
