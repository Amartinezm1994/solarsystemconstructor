/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.corpse.utils;

import com.badlogic.gdx.math.Matrix4;
import com.example.solarsystem.corpse.core.ICentereable;
import com.example.solarsystem.corpse.external.RealAttributes;
import com.example.solarsystem.corpse.movement.CorpseMovement;
import com.example.solarsystem.corpse.movement.EllipticAnimation;
import com.example.solarsystem.corpse.movement.IMovement;

public class MovementFactory {
	
	public static IMovement getMovement(CorpseMovement movementType, 
			RealAttributes realAttr, Matrix4 corpsePosition, ICentereable center) {
		switch(movementType) {
		case ELLIPTIC:
			return new EllipticAnimation(realAttr, corpsePosition, center);
		default:
			return null;
		}
	}
}
