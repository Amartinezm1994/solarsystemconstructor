/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.corpse.utils;

//import com.example.solarsystem.corpse.core.AdaptedAttributes;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.utils.Disposable;

public class CorpseModelBuilder implements Disposable {
	
	private static final int DEFAULT_DIVISIONS = 50;
	
	private ModelBuilder builder;
	private long textureAttribute;
	private long attributes;
	
	public CorpseModelBuilder() {
		builder = new ModelBuilder();
		textureAttribute = TextureAttribute.Diffuse;
		attributes = (Usage.Position | Usage.Normal | Usage.TextureCoordinates);
	}
	
	public void setTextureAttribute(String alias) {
		textureAttribute = TextureAttribute.getAttributeType(alias);
	}
	public void setTextureAttribute(long attr) {
		textureAttribute = attr;
	}
	public void setAttributes(long attributes) {
		this.attributes = attributes;
	}
	
	public Model createSphereModel(float radius, Texture textToApply) {
		Material modelMaterial = new Material(new TextureAttribute(textureAttribute, textToApply));
		return builder.createSphere(radius, radius, radius, DEFAULT_DIVISIONS, 
				DEFAULT_DIVISIONS, modelMaterial, attributes);
		//return builder.createSphere(2f, 2f, 2f, 40, 40, new Material(TextureAttribute.createDiffuse(textToApply)), Usage.Position | Usage.Normal | Usage.TextureCoordinates);
	}

	@Override
	public void dispose() {
		builder = null;
	}
	
	
	
}
