/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.corpse.core;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.example.solarsystem.camera.utils.MetaCoordinates;
import com.example.solarsystem.corpse.external.RealAttributes;
import com.example.solarsystem.corpse.movement.IMovement;

/**
 * See more in {@link ModelInstance}.
 * This class needs a corpse parameters to be identified,
 * the {@link SolarSystem} take the different parameters to renderize
 * this corpse.
 * @author Adrián Martínez Montes
 *
 */
public class Corpse extends ModelInstance implements ICentereable {
	
	private int order;
	private RealAttributes realAttributes;
	private AdaptedAttributes adaptedAttributes;
	private IMovement movement;
	
	public Corpse(Model model, RealAttributes realAttributes, 
			AdaptedAttributes adaptedAttributes, Vector3 initialPosition) {
		super(model, initialPosition);
		order = realAttributes.getOrder();
		this.realAttributes = realAttributes;
		this.adaptedAttributes = adaptedAttributes;
	}
	
	public void update(float delta) {
		if (movement != null)
			movement.update(delta);
	}
	

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public RealAttributes getRealAttributes() {
		return realAttributes;
	}

	public IMovement getMovement() {
		return movement;
	}

	public void setMovement(IMovement movement) {
		this.movement = movement;
	}
	
	public void setVelocity(float velocity) {
		movement.setVelocity(velocity);
	}

	@Override
	public Vector3 getCenter() {
		return transform.getTranslation(new Vector3());
	}

	protected AdaptedAttributes getAdaptedAttributes() {
		return adaptedAttributes;
	}

	protected void setAdaptedAttributes(AdaptedAttributes adaptedAttributes) {
		this.adaptedAttributes = adaptedAttributes;
	}
	
	/**
	 * Generates a new instance of {@link MetaCoordinates} to determine
	 * exactly the current center because of the {@link Vector3} generates
	 * a new instance everytime that request the matrix position.
	 * @return an instance of {@link MetaCoordinates}.
	 */
	public MetaCoordinates getMetaCoordinates() {
		return new MetaCoordinates(this, adaptedAttributes.radius);
	}
	
}
