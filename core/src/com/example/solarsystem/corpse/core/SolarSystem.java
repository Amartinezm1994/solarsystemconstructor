/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */package com.example.solarsystem.corpse.core;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Map.Entry;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.math.Vector3;
import com.example.solarsystem.camera.utils.MetaCoordinates;
import com.example.solarsystem.corpse.exceptions.CorpseNotFoundException;
import com.example.solarsystem.corpse.external.IRealData;
import com.example.solarsystem.corpse.external.RealAttributes;
import com.example.solarsystem.corpse.external.SolarSystemData;
import com.example.solarsystem.corpse.movement.CorpseMovement;
import com.example.solarsystem.corpse.movement.IMovement;
import com.example.solarsystem.corpse.movement.IVelocitySetteable;
import com.example.solarsystem.corpse.utils.CorpseModelBuilder;
import com.example.solarsystem.corpse.utils.MovementFactory;
import com.example.solarsystem.corpse.utils.IRealDataObtainable;
import com.example.solarsystem.utils.exceptions.AliasNotStoredException;
import com.example.solarsystem.utils.exceptions.AssetTypeNotStoredException;
import com.example.solarsystem.utils.management.IResourceManager;

//<<<<<<< HEAD

public class SolarSystem implements IRealDataObtainable, IVelocitySetteable {
//=======
/**
 * The edge of application. This class contains
 * the graphic corpses and info about it and manages
 * the obtaining methods of corpses.
 * 
 * @author Adrián Martínez Montes
 *
 */
//public class SolarSystem implements IMetaCoordinatesGetteable, IRealDataObtainable, IVelocitySetteable {
//>>>>>>> d4e36c12ad4fc9d0b53ea54c4be4ada3aa5c31fe
	
	private SolarSystemCenter center;
	private Hashtable<String, Corpse> corpses;
	private String firstCorpse;
	
	/**
	 * Construct the models based on the specified resources,
	 * if the specified alias of resources has the model, the
	 * corpse will be construct directly of model, in other case,
	 * if the model is not present, the constructor constructs the model
	 * based by specified texture by alias.
	 * 
	 * @param dataSystem the data of system.
	 * @param manager the resource manager.
	 * @throws AliasNotStoredException when the alias hasn't stored previously in the asset manager.
	 * @throws AssetTypeNotStoredException when the asset is not stored with this type.
	 */
	public SolarSystem(SolarSystemData dataSystem, IResourceManager manager) 
			throws AliasNotStoredException, AssetTypeNotStoredException {
		center = new SolarSystemCenter();
		firstCorpse = null;
		corpses = new Hashtable<String, Corpse>();
		CorpseModelBuilder builder = new CorpseModelBuilder();
		
		for (RealAttributes attr : dataSystem.getCorpseCollection()) {
			
			AdaptedAttributes adaptedAttr = new AdaptedAttributes(attr);
			String corpseName = attr.getName();
			if (firstCorpse == null) {
				firstCorpse = corpseName;
			}
			Model corpseModel;
			if (attr.isExistingResource()) {
				corpseModel = manager.getModelByAlias(attr.getResourceAlias());
			} else {
				Texture corpseTexture = manager.getTextureByAlias(attr.getResourceAlias());
				corpseModel = builder.createSphereModel(adaptedAttr.radius, corpseTexture);
			}
			Vector3 initialPosition = new Vector3(adaptedAttr.aphelion, 0f, 0f);
			Corpse createdCorpse = new Corpse(corpseModel, attr, adaptedAttr, initialPosition);
			corpses.put(corpseName, createdCorpse);
		}
	}
	
	/**
	 * Assign movements to all stored corpses, the movement is determined
	 * by the {@link CorpseMovement} enumeration.
	 * 
	 * @param movementType the type of movement.
	 */
	public void assignMovements(CorpseMovement movementType) {
		for (Entry<String, Corpse> ent : corpses.entrySet()) {
			Corpse corp = ent.getValue();
			String centerName = corp.getRealAttributes().getCenter();
			ICentereable center;
			if (centerName.equals("buit")) {
				center = this.center;
			} else {
				center = corpses.get(centerName);
			}
			 
			IMovement movement = MovementFactory.getMovement(movementType, corp.getRealAttributes(), corp.transform, center);
			corp.setMovement(movement);
		}
	}

	@Override
	public IRealData getRealAttributesCorpse(String corpseName) throws CorpseNotFoundException {
		if (!corpses.containsKey(corpseName))
			throw new CorpseNotFoundException();
		return corpses.get(corpseName).getRealAttributes();
	}

	//@Override
	public MetaCoordinates getMetaCoordinates(String corpseName) throws CorpseNotFoundException {
		if (!corpses.containsKey(corpseName))
			throw new CorpseNotFoundException();
		
		return corpses.get(corpseName).getMetaCoordinates();
	}

	@Override
	public void setVelocity(float velocity) {
		for (Corpse corp : corpses.values()) {
			corp.setVelocity(velocity);
		}
	}
	
	public void update(float delta) {
		for (Corpse corp : corpses.values()) {
			corp.update(delta);
		}
	}
	
	public Collection<Corpse> getCorpses() {
		return corpses.values();
	}
	
	public String getFirstCorpseName() {
		return firstCorpse;
	}
	
	/**
	 * The solar system have a center but didn't have corpse
	 * with this attributes, then, the class have a inner class
	 * with a {@link ICentereable} that indicates the center of
	 * solar system.
	 * 
	 * @author Adrián Martínez Montes
	 *
	 */
	private class SolarSystemCenter implements ICentereable {
		
		private Vector3 center;
		
		public SolarSystemCenter() {
			center = new Vector3(0, 0, 0);
		}
		
		@Override
		public Vector3 getCenter() {
			return new Vector3(center);
		}
	}
}
