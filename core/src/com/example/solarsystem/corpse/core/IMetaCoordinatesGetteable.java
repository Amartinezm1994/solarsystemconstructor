/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.corpse.core;

import com.example.solarsystem.camera.utils.MetaCoordinates;
import com.example.solarsystem.corpse.exceptions.CorpseNotFoundException;

/**
 * An interface to provide the {@link MetaCoordinates} of the
 * specified corpse, should be implemented by a {@link SolarSystem}.
 * 
 * @author Adrián Martínez Montes
 *
 */
public interface IMetaCoordinatesGetteable {
	
	/**
	 * Return a new instance of {@link MetaCoordinates} specified by
	 * a corpse name.
	 * 
	 * @param corpseName the corpse name of desired coordinates.
	 * @return a new instance of MetaCoordinates.
	 * @throws CorpseNotFoundException when the specified corpse not found.
	 */
	public MetaCoordinates getMetaCoordinates(String corpseName) throws CorpseNotFoundException;
}
