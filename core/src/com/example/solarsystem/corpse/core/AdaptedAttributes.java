/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.corpse.core;

import com.example.solarsystem.corpse.external.IRealData;

/**
 * This class contains the adapted attributes
 * of some corpse, all attributes has been adapted
 * to run correctly in the LibGDX 3D engine and
 * respecting the proportions of real attributes of
 * corpse.
 * 
 * @author Adrián Martínez Montes
 *
 */
public class AdaptedAttributes {
	
	public final float radius;
	public final float density;
	public final float mass;
	public final float volume;
	public final float inclination;
	public final float aphelion;
	public final float orbitRadius;
	public final float excentricity;
	public final float orbitalPeriod;
	public final float sideralPeriod;
	
	public final boolean horaryRotation;
	public final float orbitMoment;
	
	public static final float DIVISOR = 100000000f;
	
	public AdaptedAttributes(IRealData attr) {
		this.radius = (float) (attr.getRadius() / DIVISOR);
		this.density = (float) (attr.getDensity() / DIVISOR);
		this.mass = (float) (attr.getMass() / DIVISOR);
		this.volume = (float) (attr.getVolume() / DIVISOR);
		this.inclination = (float) attr.getInclination();
		this.aphelion = (float) (attr.getAphelion() / DIVISOR);
		this.orbitRadius = (float) (attr.getOrbitRadius() / DIVISOR);
		this.excentricity = (float)attr.getExcentricity();
		this.orbitalPeriod = (float) (attr.getOrbitalPeriod());
		this.sideralPeriod = (float)attr.getSideralPeriod();
		this.horaryRotation = attr.isHoraryRotation();
		this.orbitMoment = (float) attr.getOrbitMoment();
		//System.out.println(attr.getName() + " " + orbitalPeriod);
	}
	
}
