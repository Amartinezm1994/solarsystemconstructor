/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.corpse.movement;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.example.solarsystem.corpse.core.AdaptedAttributes;
import com.example.solarsystem.corpse.core.ICentereable;
import com.example.solarsystem.corpse.external.RealAttributes;

/**
 * This class describes an elliptic animation
 * 
 * 
 * @author Adrián Martínez Montes
 *
 */
public class EllipticAnimation implements IMovement {
	
	private static final int MAXIUM_PERCENTAGE = 100;
	private static final int DEFAULT_POINTS_ELLIPSE = 50;
	private static final double VELOCITY_POW = 4.1;
	
	private AdaptedAttributes adAttr;
	
	private ICentereable centereable;
	private Matrix4 position;
	private final Matrix4 initialPosition;
	
	private float velocity;
	private float actualPercentage;
	
	public EllipticAnimation(final RealAttributes attributes, 
			Matrix4 position, ICentereable centereable) {
		this.centereable = centereable;
		this.position = position;
		
		initialPosition = new Matrix4(position);
		adAttr = new AdaptedAttributes(attributes);
		velocity = adAttr.orbitalPeriod;
		actualPercentage = adAttr.orbitMoment;
	}
	
	@Override
	public void update(float delta) {
		float timeToAdvance = delta / velocity/* * (adAttr.orbitalPeriod * DEFAULT_POINTS_ELLIPSE)*/;
		float angVelocity = (float) (2 * Math.PI / timeToAdvance);
		
		float angularMoment = actualPercentage / MAXIUM_PERCENTAGE * (angVelocity * timeToAdvance);
		float degreeInclination = (float) (adAttr.inclination * Math.PI * 2 / 360);
		
		Vector3 positionV3 = position.getTranslation(new Vector3());
		Vector3 center = centereable.getCenter();
		
		float x = (float) (adAttr.orbitRadius * Math.cos(angularMoment) * Math.cos(degreeInclination) 
				- (adAttr.excentricity * adAttr.orbitRadius * Math.sin(angularMoment) * Math.sin(degreeInclination)) 
				+ center.x);
		float y = (float) (adAttr.orbitRadius * Math.cos(angularMoment) * Math.sin(degreeInclination) 
				+ (adAttr.excentricity * adAttr.orbitRadius * Math.sin(angularMoment) * Math.cos(degreeInclination)) 
				+ center.y);
		Vector3 newPosition = new Vector3(x/* + positionV3.x*/, y/* + positionV3.y*/, center.z);
		position.setTranslation(newPosition);
		//position.translate(newPosition);
		if (actualPercentage + (timeToAdvance) > MAXIUM_PERCENTAGE) {
			actualPercentage = actualPercentage + (timeToAdvance) - MAXIUM_PERCENTAGE;
		} else {
			actualPercentage += timeToAdvance;
		}
	}

	@Override
	public void setVelocity(float velocity) {
		this.velocity = (float) (adAttr.orbitalPeriod / Math.pow(velocity, VELOCITY_POW));
	}

	@Override
	public void reset() {
		position.set(initialPosition);
		actualPercentage = adAttr.orbitMoment;
	}

}
