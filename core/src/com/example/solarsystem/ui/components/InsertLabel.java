/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.ui.components;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class InsertLabel extends Label {
	
	private static final String SEPARATOR = ": ";
	
	private String header;
	private String measure;

	public InsertLabel(String header, String measure, Skin skin) {
		super(header + SEPARATOR + measure, skin);
		if (measure == null)
			measure = "";
		this.header = header;
		this.measure = measure;
	}
	
	public void insertValue(String value) {
		setText(header + SEPARATOR + value + " " + measure);
	}
}
