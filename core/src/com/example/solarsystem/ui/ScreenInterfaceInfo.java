/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.ui;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.example.solarsystem.app.utils.IAppInfo;
import com.example.solarsystem.corpse.external.IRealData;
import com.example.solarsystem.screen.AbstractScreen;
import com.example.solarsystem.ui.components.InsertLabel;
import com.example.solarsystem.ui.utils.IInfoSetteable;
import com.example.solarsystem.utils.exceptions.AliasNotStoredException;
import com.example.solarsystem.utils.exceptions.AssetTypeNotStoredException;

/**
 * 
 * @author adrian
 *
 */
public class ScreenInterfaceInfo extends AbstractScreen implements IInfoSetteable {
	
	private List<Label> labelList;
	private Hashtable<String, java.util.List<String>> propertyFields;
	private Hashtable<String, InsertLabel> labelIdentified;
	
	private TextButton buttonBack;
	
	private Stage stage;
	
	private Skin uiSkin;
	
	public ScreenInterfaceInfo(IAppInfo infoPack, Hashtable<String, java.util.List<String>> propertyFields) {
		super(infoPack);
		this.propertyFields = propertyFields;
	}
	
	public void initScreen() throws AliasNotStoredException, AssetTypeNotStoredException {
		
		uiSkin = infoPack.manager().getSkinByAlias(UIConstants.SKIN_ALIAS);
		
		stage = new Stage();
		
		int marginLeftRight = infoPack.screenWidth() / 10;
		int horizontalMargins = infoPack.screenHeight() / 20;
		int buttonWidth = infoPack.screenWidth() / 4;
		int buttonHeight = infoPack.screenHeight() / 10;
		
		buttonBack = new TextButton(UIConstants.BUTTON_BACK_TEXT, uiSkin);
		buttonBack.setWidth(buttonWidth);
		buttonBack.setHeight(buttonHeight);
		buttonBack.setPosition(marginLeftRight, horizontalMargins);
		buttonBack.addListener(new ButtonBackListener());
		stage.addActor(buttonBack);
		
		labelList = new List<Label>(uiSkin);
		
		Array<Label> labelsToAdd = new Array<Label>();
		labelIdentified = new Hashtable<String, InsertLabel>();
		for (Entry<String, java.util.List<String>> toInsertAttr : propertyFields.entrySet()) {
			Iterator<String> iter = toInsertAttr.getValue().iterator();
			String measure = iter.next();
			String field = iter.next();
			InsertLabel labelToIns = new InsertLabel(field, measure, uiSkin);
			//System.out.println(labelToIns.getText());
			//Container<Label> containerLabel = new Container<Label>(labelToIns);
			//containerLabel.setColor(Color.BLUE);
			//containerLabel.set
			//containerLabel.setBackground(background);
			//labelList.
			labelsToAdd.add(labelToIns);
			labelIdentified.put(toInsertAttr.getKey(), labelToIns);
		}
		labelList.setItems(labelsToAdd);
		
		//stage.addActor(labelList);
		//labelList.
		
		ScrollPane scrollAttributes = new ScrollPane(labelList);
		
		scrollAttributes.setWidth(infoPack.screenWidth() - marginLeftRight * 2);
		scrollAttributes.setHeight(infoPack.screenHeight() - horizontalMargins * 3 - buttonHeight);
		scrollAttributes.setPosition(marginLeftRight, horizontalMargins * 2 + buttonHeight);
		stage.addActor(scrollAttributes);
		
		//labelList.get
	}
	
	@Override
	public void setFocusedCorpse(IRealData attributes) {
		try {
			for (Entry<String, String> values : attributes.getOrderedAttributes().entrySet()) {
				labelIdentified.get(values.getKey()).insertValue(values.getValue());
				//System.out.println(labelIdentified.get(values.getKey()).getText());
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			infoPack.changer().showError("Error when obtaining the attributes of the selected corpse.", e);
		}
	}

	@Override
	public void show() {
		
	}

	@Override
	public void render(float delta) {
		stage.act();
		stage.draw();
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public InputProcessor getInputProcessor() {
		return stage;
	}
	
	private class ButtonBackListener extends ClickListener {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			infoPack.changer().showVisor();
			super.clicked(event, x, y);
		}
	}
}
