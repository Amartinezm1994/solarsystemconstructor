/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.ui;

import java.util.ArrayList;
import java.util.List;

import com.example.solarsystem.corpse.exceptions.CorpseNotFoundException;
import com.example.solarsystem.visor.ICorpseFocalizable;
import com.example.solarsystem.visor.ICorpseSetteable;

public class FocusIntermediate implements ICorpseFocalizable {
	
	private ICorpseFocalizable realFocalizable;
	private List<ICorpseSetteable> setteables;
	
	public FocusIntermediate(ICorpseFocalizable realFocalizable, List<ICorpseSetteable> setteables) {
		this.realFocalizable = realFocalizable;
		this.setteables = setteables;
	}
	public FocusIntermediate(ICorpseFocalizable realFocalizable) {
		this(realFocalizable, new ArrayList<ICorpseSetteable>());
	}
	
	public void addSetteable(ICorpseSetteable setteable) {
		setteables.add(setteable);
	}
	
	@Override
	public void setFocusOn(String corpseName) throws CorpseNotFoundException {
		realFocalizable.setFocusOn(corpseName);
		if (!setteables.isEmpty()) {
			for (ICorpseSetteable setteable : setteables) {
				setteable.setFocusOn(corpseName);
			}
		}
	}

	@Override
	public String getCorpseFocus() {
		return realFocalizable.getCorpseFocus();
	}
}
