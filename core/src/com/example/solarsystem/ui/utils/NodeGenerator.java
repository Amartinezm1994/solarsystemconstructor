/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.ui.utils;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Tree.Node;
import com.example.solarsystem.corpse.external.RealAttributes;
import com.example.solarsystem.corpse.external.SolarSystemData;

public class NodeGenerator {
	
	/**
	 * The first dimension indicates the level of corpse, the second
	 * dimension is a pair of strings, the first string indicates the
	 * name of corpse and the second string indicates which corpse
	 * have the second corpse.
	 */
	private Hashtable<Integer, List<String[]>> corpseLevelsAsList;
	private Integer innerLevel;
	private String textFirstLabel;
	
	public NodeGenerator(final SolarSystemData system, String textFirstLabel/*, Skin uiSkin*/) {
		corpseLevelsAsList = new Hashtable<Integer, List<String[]>>();
		innerLevel = new Integer(0);
		this.textFirstLabel = textFirstLabel;
		for (RealAttributes corpse : system.getCorpseCollection()) {
			Integer corpseLevel = new Integer(corpse.getOrder());
			String centerName = corpse.getCenter();
			if (centerName == null) {
				centerName = textFirstLabel;
			}
			String[] corpseNode = new String[]{centerName, corpse.getName()};
			List<String[]> levelAllCorpses;
			if (corpseLevelsAsList.containsKey(corpseLevel)) {
				levelAllCorpses = corpseLevelsAsList.get(corpseLevel);
			} else {
				levelAllCorpses = new ArrayList<String[]>();
				corpseLevelsAsList.put(corpseLevel, levelAllCorpses);
			}
			if (innerLevel.compareTo(corpseLevel) < 0) {
				innerLevel = new Integer(corpseLevel.intValue());
			}
			levelAllCorpses.add(corpseNode);
		}
	}
	
	/*public Node generateTreeNodeAsLabels(Skin uiSkin, String textFirstLabel, float boxWidth, 
			float boxHeight, float rightMargin, float subLevelRightMargin, float horizontalMargin) {
		//float rightMargin = boxWidth / 20;
		//float subLevelRightMargin = boxWidth / 10;
		//float horizontalMargin = boxHeight / 20;
		List<Label> labelList = new ArrayList<Label>();
		labelList.add(new Label(textFirstLabel, uiSkin));
		for (Entry<Integer, List<String[]>> entryLevel : corpseLevelsAsList.entrySet()) {
			for (String[] corpseInfo : entryLevel.getValue()) {
				
				//labelCorpse.setWidth(boxWidth - rightMargin - subLevelRightMargin * entryLevel.getKey()); 
				//labelCorpse.setHeight(height);
			}
		}
		return generateTreeNodeAsLabels(labelList);
	}*/
	public Node generateTreeNodeAsLabels(Skin uiSkin/*,  float boxWidth, 
			float boxHeight, float rightMargin, float subLevelRightMargin, float horizontalMargin*/) {
		//ActorCloneable act = new ActorCloneable();
		//try {
		//	act.clone();
		//} catch (CloneNotSupportedException e) {
		//	e.printStackTrace();
		//}
		//act.
		Node primordialNode = new Node(new Label(textFirstLabel, uiSkin));
		//primordialNode.
		Hashtable<String, Node> hashtableNodesParent = new Hashtable<String, Node>();
		hashtableNodesParent.put(textFirstLabel, primordialNode);
		Hashtable<String, Node> hashtableNodesCurrent = new Hashtable<String, Node>();
		Node parentNode = primordialNode;
		for (Integer nodeLevel = new Integer(1); 
				nodeLevel.compareTo(innerLevel.intValue()) <= 0; 
				nodeLevel = new Integer(nodeLevel.intValue() + 1)) {
			
			for (String[] corpseInLevel : corpseLevelsAsList.get(nodeLevel)) {
				//System.out.println(corpseInLevel[1] + " " + corpseInLevel[0]);
				parentNode = hashtableNodesParent.get(corpseInLevel[0]);
				Label labelCorpse = new Label(corpseInLevel[1], uiSkin);
				labelCorpse.setName(corpseInLevel[1]);
				Node levelNode = new Node(labelCorpse);
				hashtableNodesCurrent.put(corpseInLevel[1], levelNode);
				parentNode.add(levelNode);
				//labelList.add(labelCorpse);
			}
			
			hashtableNodesParent = hashtableNodesCurrent;
			hashtableNodesCurrent = new Hashtable<String, Node>();
		}
		/*for (Entry<Integer, List<String[]>> entryLevel : corpseLevelsAsList.entrySet()) {
			primordialNode.
		}
		corpseLevelsAsList.*/
		return primordialNode;
	}
	
	/*private Node getNode(List<Label> labelList, Integer currentLevel) {
		
	}*/
	
	/*public static Node getNodeTreeOfSolarSystem(final SolarSystemData system, Skin uiSkin) {
		Collection<RealAttributes> corpses = system.getCorpseCollection();
		
		//RealAttri
		//Iterator<RealAttributes> iter = corpses.iterator();
		//corpses.
		//corpses.
		//while(iter.hasNext()) {
		//	RealAttributes attr = iter.next();
		//	attr.getCenter();
			//iter.
			//iter.
		//}
		re*turn null;
	}*/
	
	/*private class ActorCloneable extends Actor implements Cloneable {
		@Override
		protected Object clone() throws CloneNotSupportedException {
			// TODO Auto-generated method stub
			return super.clone();
		}
	}*/
}
