/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.ui;

import java.util.List;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Tree;
import com.badlogic.gdx.scenes.scene2d.ui.Tree.Node;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.example.solarsystem.app.utils.IAppInfo;
import com.example.solarsystem.corpse.exceptions.CorpseNotFoundException;
import com.example.solarsystem.screen.AbstractScreen;
import com.example.solarsystem.ui.utils.NodeGenerator;
import com.example.solarsystem.utils.exceptions.AliasNotStoredException;
import com.example.solarsystem.utils.exceptions.AssetTypeNotStoredException;
import com.example.solarsystem.visor.ICorpseFocalizable;

public class ScreenInterfaceMenu extends AbstractScreen {
	
	private ICorpseFocalizable focalizable;
	//private ScreenChangeable changeable;
	
	private NodeGenerator nodeGen;
	private List<String> corpseList;
	
	private Tree treeSolarSystem;
	private TextButton buttonSelect;
	private TextButton buttonBack;
	private Label labelSelection;
	
	private Skin uiSkin;
	private TextureRegion backgroundTexture;
	
	private Stage stage;
	
	private String selectedCorpse;

	public ScreenInterfaceMenu(IAppInfo infoPack, ICorpseFocalizable focalizable, 
			NodeGenerator nodeGen, List<String> corpseList) {
		super(infoPack);
		this.focalizable = focalizable;
		//this.changeable = changeable;
		this.nodeGen = nodeGen;
		this.corpseList = corpseList;
		
		
		//treeSolarSystem.set
	}
	
	public void initScreen() throws AliasNotStoredException, AssetTypeNotStoredException {
		//try {
		uiSkin = this.infoPack.manager().getSkinByAlias(UIConstants.SKIN_ALIAS);
		Texture textureBg = infoPack.manager().getTextureByAlias(UIConstants.TEXTURE_BG1);
		backgroundTexture = new TextureRegion(textureBg);
		
		int definedScreenWidth = infoPack.screenWidth() * 2;
		int definedScreenHeight = infoPack.screenHeight() * 2;
		//System.out.println(definedScreenWidth + " " + definedScreenHeight);
		backgroundTexture.setRegion(textureBg.getWidth() / 2 - definedScreenWidth / 2, 
				textureBg.getHeight() / 2 - definedScreenHeight / 2, 
				definedScreenWidth, definedScreenHeight);
		//System.out.println(backgroundTexture.getRegionX() + " " + backgroundTexture.getRegionY() +
		//		" " + backgroundTexture.getRegionWidth() + " " + backgroundTexture.getRegionHeight());
		//} catch (AliasNotStoredException | AssetTypeNotStoredException e) {
		//	this.changeable.showError("Error on load the skin.", e);
		//}
		
		stage = new Stage();
		
		int marginLeftRight = infoPack.screenWidth() / 10;
		int horizontalMargins = infoPack.screenHeight() / 20;
		int buttonWidth = infoPack.screenWidth() / 4;
		int buttonHeight = infoPack.screenHeight() / 10;
		
		
		
		treeSolarSystem = new Tree(uiSkin);
		/*treeSolarSystem.setWidth(infoPack.screenWidth() - marginLeftRight * 2);
		treeSolarSystem.setHeight(infoPack.screenHeight() - (buttonHeight + horizontalMargins * 3));
		treeSolarSystem.setPosition(marginLeftRight, buttonHeight + horizontalMargins * 2);*/
		
		
		ScrollPane scrollPane = new ScrollPane(treeSolarSystem);
		
		scrollPane.setWidth(infoPack.screenWidth() - marginLeftRight * 2);
		scrollPane.setHeight(infoPack.screenHeight() - (buttonHeight + horizontalMargins * 3));
		scrollPane.setPosition(marginLeftRight, buttonHeight + horizontalMargins * 2);
		//treeSolarSystem.add
		//treeSolarSystem.add
		stage.addActor(scrollPane);
		//NodeGenerator nodeGen = new NodeGenerator(solarData, textFirstLabel)
		Node primordialNode = nodeGen.generateTreeNodeAsLabels(uiSkin);
		treeSolarSystem.add(primordialNode);
		//treeSolarSystem.get
		//stage.addActor(treeSolarSystem);
		treeSolarSystem.addListener(new TreeListener());
		//treeSolarSystem.add
		
		buttonSelect = new TextButton(UIConstants.BUTTON_SELECT_TEXT, uiSkin);
		buttonSelect.setWidth(buttonWidth);
		buttonSelect.setHeight(buttonHeight);
		buttonSelect.setPosition(marginLeftRight, horizontalMargins);
		buttonSelect.addListener(new SelectButtonListener());
		stage.addActor(buttonSelect);
		
		//labelSelection = new Label("", uiSkin);
		labelSelection = new Label(/*UIConstants.LABEL_SELECTION + ":\n" + "aaa"*/"", uiSkin);
		labelSelection.setWidth(infoPack.screenWidth() - (marginLeftRight * 4 + buttonWidth * 2));
		labelSelection.setHeight(buttonHeight);
		labelSelection.setPosition(buttonSelect.getX() + buttonSelect.getWidth() + marginLeftRight, horizontalMargins);
		
		stage.addActor(labelSelection);
		
		buttonBack = new TextButton(UIConstants.BUTTON_BACK_TEXT, uiSkin);
		buttonBack.setWidth(buttonWidth);
		buttonBack.setHeight(buttonHeight);
		buttonBack.setPosition(infoPack.screenWidth() - marginLeftRight - buttonWidth, horizontalMargins);
		buttonBack.addListener(new BackButtonListener());
		stage.addActor(buttonBack);
		
		
	}

	@Override
	public void show() {
		treeSolarSystem.expandAll();
		//labelSelection.setText(UIConstants.LABEL_SELECTION + ":\n" + focalizable.getCorpseFocus());
		labelSelection.setText(/*UIConstants.LABEL_SELECTION + ":\n" + */focalizable.getCorpseFocus());
	}

	@Override
	public void render(float delta) {
		//infoPack.
		infoPack.spriteBatch().begin();
		infoPack.spriteBatch().draw(backgroundTexture, 0, 0, infoPack.screenWidth(), infoPack.screenHeight());
		infoPack.spriteBatch().end();
		stage.act();
		stage.draw();
		//stage.c
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public InputProcessor getInputProcessor() {
		return stage;
	}
	
	private class SelectButtonListener extends ClickListener {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			if (corpseList.contains(selectedCorpse)) {
				try {
					focalizable.setFocusOn(selectedCorpse);
					infoPack.changer().showVisor();
				} catch (CorpseNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private class BackButtonListener extends ClickListener {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			infoPack.changer().showVisor();
		}
	}
	
	private class TreeListener extends ClickListener {
		
		@Override
		public void clicked(InputEvent event, float x, float y) {
			
			Label corpseLabel = null;
			Object solarTree = event.getTarget();
			if (solarTree != null) {
				if (solarTree instanceof Tree) {
					Node nodeAt = ((Tree)solarTree).getNodeAt(y);
					if (nodeAt != null) {
						Object actorEvent = nodeAt.getActor();
						if (actorEvent != null && actorEvent instanceof Label) {
							corpseLabel = (Label)actorEvent;
						}
					}
					//Label labelCorpse = (Label)solarTree.getNodeAt(y).getActor();
				} else if (solarTree instanceof Label) {
					corpseLabel = (Label)event.getTarget();
				}
				if (corpseLabel != null) {
					infoPack.debugLog().debug("Selected " + corpseLabel.getText());
					selectedCorpse = corpseLabel.getText().toString();
				}
			}
				
					//if (corpseLabel.getText() != )
					
					/*try {
						focalizable.setFocusOn(labelCorpse.getText().toString());
					} catch (CorpseNotFoundException e) {
						e.printStackTrace();
					}*/
			
			//super.clicked(event, x, y);
		}
	}
}
