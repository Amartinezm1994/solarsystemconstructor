/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.camera;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;
import com.example.solarsystem.camera.utils.MetaCoordinates;
import com.example.solarsystem.corpse.core.ICentereable;

/**
 * This interface is provided to be used
 * by the environment to show it.
 * 
 * @author Adrián Martínez Montes
 *
 */
public interface ISpecificCamera {
	/**
	 * Updates the camera by delta time.
	 * @param delta the time of processor.
	 */
	public void update(float delta);
	
	/**
	 * Reset the position of camera.
	 */
	public void reset();
	
	/**
	 * Move the camera by specified coordinates and velocity.
	 * @param x coordinate.
	 * @param y coordinate.
	 * @param deltaX x velocity.
	 * @param deltaY y velocity.
	 */
	public void move(float x, float y, float deltaX, float deltaY);
	
	/**
	 * Set the zoom by specified a distance.
	 * 
	 * @param distance
	 */
	public void zoom(float distance);
	
	/**
	 * The center of a object need to be a new instance,
	 * but need a object radius also, {@link MetaCoordinates}
	 * provides this two parameters, the first is the object
	 * radius and the second is a {@link ICentereable}, this object
	 * generates a new instance of {@link Vector3} that indicates 
	 * the point center of object.
	 * @param coordinates
	 */
	public void setObjective(MetaCoordinates coordinates);
	
	/**
	 * Returns the original camera.
	 * @return the original camera.
	 */
	public Camera getCamera();
}
