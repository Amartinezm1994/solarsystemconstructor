/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.camera;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;
import com.example.solarsystem.camera.utils.MetaCoordinates;
import com.example.solarsystem.corpse.core.ICentereable;
import com.example.solarsystem.utils.math.LessDistanceException;
import com.example.solarsystem.utils.math.MathFunctions;
import com.example.solarsystem.utils.math.MoreDistanceException;

/**
 * Implements a camera and the functions as
 * zoom or move around of the specified
 * {@link ICentereable}.
 * @author Adrián Martínez Montes
 *
 */
public class AroundCamera extends PerspectiveCamera implements ISpecificCamera {
	
	private ICentereable objective;
	private Vector3 vectorDirection;
	private float objectiveRadius;
	private static final float MINIMAL_VECTOR = 1f;
	private static final float MAXIUM_DISTANCE = 80f;
	private static final float ANGLE_MOVE = 3f;
	private static final float FIELD_CAMERA = 1;
	
	public AroundCamera(float viewportWidth, float viewportHeight) {
		super(FIELD_CAMERA, viewportWidth, viewportHeight);
	}

	@Override
	public void update(float delta) {
		Vector3 objective = this.objective.getCenter();
		position.x = objective.x + vectorDirection.x;
		position.y = objective.y + vectorDirection.y;
		position.z = objective.z + vectorDirection.z;
		update();
	}

	@Override
	public void reset() {
		setPrimalPosition(objective.getCenter());
	}

	@Override
	public void zoom(float distance) {
		Vector3 objective = this.objective.getCenter();
		Vector3 newPosition = MathFunctions.sumateRealDistance(position, vectorDirection, distance);
		try {
			MathFunctions.distanceForZoom(objective, newPosition, objectiveRadius, MINIMAL_VECTOR, MAXIUM_DISTANCE);
			position.x = newPosition.x;
			position.y = newPosition.y;
			position.z = newPosition.z;
			vectorDirection = MathFunctions.directorVector(objective, position);
		} catch (LessDistanceException e) {
			vectorDirection = MathFunctions.vectorToDistance(vectorDirection, 
					+ objectiveRadius + MINIMAL_VECTOR);
			position.x = objective.x + vectorDirection.x;
			position.y = objective.y + vectorDirection.y;
			position.z = objective.z + vectorDirection.z;
		} catch (MoreDistanceException e) {
			vectorDirection = MathFunctions.vectorToDistance(vectorDirection, 
					+ objectiveRadius + MAXIUM_DISTANCE);
			position.x = objective.x + vectorDirection.x;
			position.y = objective.y + vectorDirection.y;
			position.z = objective.z + vectorDirection.z;
		}
	}
	
	/**
	 * Reset the camera and updates the direction vector.
	 * 
	 * @param objective
	 */
	private void setPrimalPosition(Vector3 objective) {
		this.position.x = objective.x;
		this.position.y = (float) (objective.y + objectiveRadius + MINIMAL_VECTOR);
		this.position.z = objective.z;
		lookAt(objective);
		vectorDirection = MathFunctions.directorVector(objective, position);
	}

	@Override
	public void setObjective(MetaCoordinates coordinates) {
		objective = coordinates.center;
		Vector3 objective = this.objective.getCenter();
		this.objectiveRadius = coordinates.radius;
		setPrimalPosition(objective);
	}

	@Override
	public Camera getCamera() {
		return this;
	}

	@Override
	public void move(float x, float y, float deltaX, float deltaY) {
		Vector3 objective = this.objective.getCenter();
		Vector3 newVector = new Vector3(position);
		newVector.x += deltaX;
		newVector.y += deltaY;
		rotateAround(objective, newVector, ANGLE_MOVE);
		vectorDirection = MathFunctions.directorVector(objective, position);
		
	}
	
}
