/**
 * 	This file is part of SolarSystemConstructor3D.
 *
 *  SolarSystemConstructor3D is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SolarSystemConstructor3Dobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SolarSystemConstructor3D.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.example.solarsystem.camera.utils;

import com.example.solarsystem.corpse.core.ICentereable;

/**
 * This class exists because need a two objects and
 * we not want modify this attributes because this
 * class is generated everytime that the camera needs
 * the center.
 * 
 * @author Adrián Martínez Montes
 *
 */
public final class MetaCoordinates {
	public final ICentereable center;
	public final float radius;
	
	public MetaCoordinates(ICentereable center, float radius) {
		this.center = center;
		this.radius = radius;
	}
}
