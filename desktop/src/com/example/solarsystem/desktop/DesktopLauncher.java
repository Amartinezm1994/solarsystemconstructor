package com.example.solarsystem.desktop;


import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.example.solarsystem.app.SolarSystemConstructor;
import com.example.solarsystem.app.utils.BundleInformation;
import com.example.solarsystem.corpse.external.AttributeProperties;
import com.example.solarsystem.corpse.external.CorpseType;
import com.example.solarsystem.corpse.external.RealAttributes;
import com.example.solarsystem.corpse.external.SolarSystemData;

public class DesktopLauncher {
	public static void main (String[] arg) throws Exception {
		RealAttributes attr1 = new RealAttributes();
		//attr1.setOrder(1);
		attr1.setAphelion(20000);
		attr1.setCenter("buit");
		attr1.setCorpseType(CorpseType.STAR);
		attr1.setDensity(40000);
		attr1.setExcentricity(0.8);
		attr1.setHoraryRotation(false);
		attr1.setInclination(80);
		attr1.setMass(5000000);
		attr1.setName("Sol");
		attr1.setOrbitalPeriod(60);
		attr1.setOrbitMoment(1);
		attr1.setOrbitRadius(1);
		attr1.setOrder(1);
		attr1.setRadius(500000);
		attr1.setResourceAlias("Ako");
		attr1.setSideralPeriod(60000);
		attr1.setVolume(600000);
		//attr1.set
		
		RealAttributes attr2 = new RealAttributes();
		//attr2.setOrder(2);
		attr2.setAphelion(200000);
		attr2.setCenter("Sol");
		attr2.setCorpseType(CorpseType.ASTEROID);
		attr2.setDensity(40000);
		attr2.setExcentricity(0.8);
		attr2.setHoraryRotation(false);
		attr2.setInclination(80);
		attr2.setMass(5000000);
		attr2.setName("Tierra");
		attr2.setOrbitalPeriod(1);
		attr2.setOrbitMoment(1);
		attr2.setOrbitRadius(600);
		attr2.setOrder(2);
		attr2.setRadius(100000);
		attr2.setResourceAlias("Dante");
		attr2.setSideralPeriod(60000);
		attr2.setVolume(600000);
		
		RealAttributes attr3 = new RealAttributes();
		//attr2.setOrder(2);
		attr3.setAphelion(400000);
		attr3.setCenter("Sol");
		attr3.setCorpseType(CorpseType.ASTEROID);
		attr3.setDensity(40000);
		attr3.setExcentricity(0.8);
		attr3.setHoraryRotation(false);
		attr3.setInclination(80);
		attr3.setMass(5000000);
		attr3.setName("Toc");
		attr3.setOrbitalPeriod(1);
		attr3.setOrbitMoment(1);
		attr3.setOrbitRadius(3000);
		attr3.setOrder(2);
		attr3.setRadius(300000);
		attr3.setResourceAlias("Dante");
		attr3.setSideralPeriod(60000);
		attr3.setVolume(600000);
		
		RealAttributes attr4 = new RealAttributes();
		//attr2.setOrder(2);
		attr4.setAphelion(20000);
		attr4.setCenter("Tierra");
		attr4.setCorpseType(CorpseType.ASTEROID);
		attr4.setDensity(40000);
		attr4.setExcentricity(0.8);
		attr4.setHoraryRotation(false);
		attr4.setInclination(80);
		attr4.setMass(5000000);
		attr4.setName("Toca");
		attr4.setOrbitalPeriod(6000);
		attr4.setOrbitMoment(70);
		attr4.setOrbitRadius(5000);
		attr4.setOrder(3);
		attr4.setRadius(500000);
		attr4.setResourceAlias("Dante");
		attr4.setSideralPeriod(60000);
		attr4.setVolume(600000);
		
		/*RealAttributes attr5 = new RealAttributes();
		attr5.setCenter("Tierra");
		attr5.setName("Tocaaa");
		attr5.setOrder(3);
		
		RealAttributes attr6 = new RealAttributes();
		attr6.setCenter("Tierra");
		attr6.setName("Tocaaaaa");
		attr6.setOrder(3);
		
		RealAttributes attr7 = new RealAttributes();
		attr7.setCenter("Tierra");
		attr7.setName("Tocaaasda");
		attr7.setOrder(3);
		
		RealAttributes attr8 = new RealAttributes();
		attr8.setCenter("Tierra");
		attr8.setName("Tocaaasdada");
		attr8.setOrder(3);*/
		
		SolarSystemData solarData = new SolarSystemData();
		solarData.addCorpse(attr1);
		solarData.addCorpse(attr2);
		//solarData.addCorpse(attr3);
		/*solarData.addCorpse(attr4);
		solarData.addCorpse(attr5);
		solarData.addCorpse(attr6);
		solarData.addCorpse(attr7);
		solarData.addCorpse(attr8);*/
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 600;
		config.height = 600;
		BundleInformation infoBundle = new BundleInformation();
		//infoBundle.setLocale(new Locale("en"));
		//infoBundle.setAssetsPath("");
		//infoBundle.setLocalizationFile("MyBundle");
		//infoBundle.set
		infoBundle.setLoadingTextureFile("textures/Dagobah.png");
		infoBundle.setPathFormatsIndicator("configs/formats_resources.json");
		infoBundle.setPathJsonResources("configs/avaible_resources.json");
		infoBundle.setPathResourceTypes("configs/literal_classes.json");
		//solarData.generateMetadataAttributes();
		//System.out.println(attr1.getHashtableMetadata());
		//solarData.getCorpse(attr2.getName());
		//attr1.generateMetadata();
		//dSystem.out.println(attr1.getArrayAttributes());
		//System.out.println(solar);
		new LwjglApplication(new SolarSystemConstructor(solarData, infoBundle), config);
	}
}
