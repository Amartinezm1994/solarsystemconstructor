package com.example.solarsystem.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.example.solarsystem.app.SolarSystemConstructor;
import com.example.solarsystem.app.utils.BundleInformation;
import com.example.solarsystem.corpse.external.SolarSystemData;

public class AndroidLauncher extends AndroidApplication {
	
	
	
	private BundleInformation infoBundle;
	private SolarSystemData dataSystem;
	//private Bundle savedInstanceState;
	
	/*public void setSystem(SolarSystemData dataSystem) {
		this.dataSystem = dataSystem;
	}*/
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dataSystem = (SolarSystemData)getIntent().getSerializableExtra("solar");
		//dataSystem = (SolarSystemData) savedInstanceState.getSerializable()("solar");
		//savedInstanceState.getSerializable()
		//DIVISOR = 100.000f;
		
		
		/*RealAttributes attr5 = new RealAttributes();
		attr5.setCenter("Tierra");
		attr5.setName("Tocaaa");
		attr5.setOrder(3);
		
		RealAttributes attr6 = new RealAttributes();
		attr6.setCenter("Tierra");
		attr6.setName("Tocaaaaa");
		attr6.setOrder(3);
		
		RealAttributes attr7 = new RealAttributes();
		attr7.setCenter("Tierra");
		attr7.setName("Tocaaasda");
		attr7.setOrder(3);
		
		RealAttributes attr8 = new RealAttributes();
		attr8.setCenter("Tierra");
		attr8.setName("Tocaaasdada");
		attr8.setOrder(3);*/
		
		
		/*solarData.addCorpse(attr5);
		solarData.addCorpse(attr6);
		solarData.addCorpse(attr7);
		solarData.addCorpse(attr8);*/
		
		//LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		//config.width = 600;
		//config.height = 600;
		infoBundle = new BundleInformation();
		//infoBundle.setLocale(new Locale("en"));
		//infoBundle.setAssetsPath("");
		//infoBundle.setLocalizationFile("MyBundle");
		//infoBundle.set
		infoBundle.setLoadingTextureFile("textures/Dagobah.png");
		infoBundle.setPathFormatsIndicator("configs/formats_resources.json");
		infoBundle.setPathJsonResources("configs/avaible_resources.json");
		infoBundle.setPathResourceTypes("configs/literal_classes.json");
		//new LwjglApplication(new SolarSystemConstructor(solarData, infoBundle), config);
		initialize();
		//JSon
	}
	
	/*@Override
	protected void onStart() {
		super.onStart();
		
	}*/
	
	public void initialize() {
		AndroidApplicationConfiguration configg = new AndroidApplicationConfiguration();
		super.initialize(new SolarSystemConstructor(dataSystem, infoBundle), configg);
	}

	
}
