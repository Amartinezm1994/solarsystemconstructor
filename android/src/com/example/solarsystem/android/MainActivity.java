package com.example.solarsystem.android;

import com.example.solarsystem.corpse.external.CorpseType;
import com.example.solarsystem.corpse.external.RealAttributes;
import com.example.solarsystem.corpse.external.SolarSystemData;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends Activity {
	
	public void first(View v) {
		//AndroidApplicationConfiguration configg = new AndroidApplicationConfiguration();
		//initialize(new SolarSystemConstructor(getFirstExample(), infoBundle), configg);
		//AndroidLauncher andApplication = new AndroidLauncher();
		
		Intent intent = new Intent(this, AndroidLauncher.class);
		intent.putExtra("solar", getFirstExample());
		startActivity(intent);
		//andApplication.setSystem(getFirstExample());
		
		//andApplication.initialize();
	}
	public void second(View v) {
		//AndroidApplicationConfiguration configg = new AndroidApplicationConfiguration();
		//initialize(new SolarSystemConstructor(getSecondExample(), infoBundle), configg);
		//AndroidLauncher andApplication = new AndroidLauncher();
		Intent intent = new Intent(this, AndroidLauncher.class);
		intent.putExtra("solar", getSecondExample());
		startActivity(intent);
		//start
		//andApplication.setSystem(getSecondExample());
		//andApplication.
		//andApplication.initialize();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private SolarSystemData getFirstExample() {
		RealAttributes attr1 = new RealAttributes();
		//attr1.setOrder(1);
		attr1.setAphelion(2000000);
		attr1.setCenter("buit");
		attr1.setCorpseType(CorpseType.STAR);
		attr1.setDensity(40000);
		attr1.setExcentricity(0.8);
		attr1.setHoraryRotation(false);
		attr1.setInclination(80);
		attr1.setMass(50000);
		attr1.setName("Sol");
		attr1.setOrbitalPeriod(60);
		attr1.setOrbitMoment(1);
		attr1.setOrbitRadius(1);
		attr1.setOrder(1);
		attr1.setRadius(80000000);
		attr1.setResourceAlias("Sun");
		attr1.setSideralPeriod(6000000);
		attr1.setVolume(60000000);
		//attr1.set
		
		RealAttributes attr2 = new RealAttributes();
		//attr2.setOrder(2);
		attr2.setAphelion(200000);
		attr2.setCenter("Sol");
		attr2.setCorpseType(CorpseType.ASTEROID);
		attr2.setDensity(40000);
		attr2.setExcentricity(0.8);
		attr2.setHoraryRotation(false);
		attr2.setInclination(180);
		attr2.setMass(5000000);
		attr2.setName("Tierra");
		attr2.setOrbitalPeriod(31536000);
		attr2.setOrbitMoment(1);
		attr2.setOrbitRadius(147000000);
		attr2.setOrder(2);
		attr2.setRadius(6371000);
		attr2.setResourceAlias("Earth");
		attr2.setSideralPeriod(60000);
		attr2.setVolume(600000);
		
		RealAttributes attr3 = new RealAttributes();
		//attr2.setOrder(2);
		attr3.setAphelion(400000);
		attr3.setCenter("Sol");
		attr3.setCorpseType(CorpseType.ASTEROID);
		attr3.setDensity(40000);
		attr3.setExcentricity(0.6);
		attr3.setHoraryRotation(false);
		attr3.setInclination(80);
		attr3.setMass(5000000);
		attr3.setName("Jol");
		attr3.setOrbitalPeriod(3153600);
		attr3.setOrbitMoment(1);
		attr3.setOrbitRadius(213000000);
		attr3.setOrder(2);
		attr3.setRadius(4000000);
		attr3.setResourceAlias("Dwon");
		attr3.setSideralPeriod(60000);
		attr3.setVolume(600000);
		
		RealAttributes attr4 = new RealAttributes();
		//attr2.setOrder(2);
		attr4.setAphelion(20000);
		attr4.setCenter("Tierra");
		attr4.setCorpseType(CorpseType.ASTEROID);
		attr4.setDensity(40000);
		attr4.setExcentricity(0.4);
		attr4.setHoraryRotation(false);
		attr4.setInclination(80);
		attr4.setMass(5000000);
		attr4.setName("Toca");
		attr4.setOrbitalPeriod(81536000);
		attr4.setOrbitMoment(1);
		attr4.setOrbitRadius(20000000);
		attr4.setOrder(3);
		attr4.setRadius(1000000);
		attr4.setResourceAlias("Dagobah");
		attr4.setSideralPeriod(60000);
		attr4.setVolume(600000);
		
		RealAttributes attr5 = new RealAttributes();
		//attr2.setOrder(2);
		attr5.setAphelion(20000);
		attr5.setCenter("Sol");
		attr5.setCorpseType(CorpseType.ASTEROID);
		attr5.setDensity(40000);
		attr5.setExcentricity(0.3);
		attr5.setHoraryRotation(false);
		attr5.setInclination(5);
		attr5.setMass(5000000);
		attr5.setName("Plutinus");
		attr5.setOrbitalPeriod(121536000);
		attr5.setOrbitMoment(1);
		attr5.setOrbitRadius(712000000);
		attr5.setOrder(2);
		attr5.setRadius(9000000);
		attr5.setResourceAlias("Quom");
		attr5.setSideralPeriod(60000);
		attr5.setVolume(600000);
		
		SolarSystemData solarData = new SolarSystemData();
		solarData.addCorpse(attr1);
		solarData.addCorpse(attr2);
		solarData.addCorpse(attr3);
		solarData.addCorpse(attr4);
		solarData.addCorpse(attr5);
		
		return solarData;
	}
	
	private SolarSystemData getSecondExample() {
		RealAttributes attr1 = new RealAttributes();
		//attr1.setOrder(1);
		attr1.setAphelion(2000000);
		attr1.setCenter("buit");
		attr1.setCorpseType(CorpseType.STAR);
		attr1.setDensity(40000);
		attr1.setExcentricity(0.8);
		attr1.setHoraryRotation(false);
		attr1.setInclination(80);
		attr1.setMass(50000);
		attr1.setName("Sol");
		attr1.setOrbitalPeriod(60);
		attr1.setOrbitMoment(1);
		attr1.setOrbitRadius(1);
		attr1.setOrder(1);
		attr1.setRadius(80000000);
		attr1.setResourceAlias("Sun");
		attr1.setSideralPeriod(6000000);
		attr1.setVolume(60000000);
		
		RealAttributes attr2 = new RealAttributes();
		//attr1.setOrder(1);
		attr2.setAphelion(2000000);
		attr2.setCenter("Sol");
		attr2.setCorpseType(CorpseType.PLANET);
		attr2.setDensity(40000);
		attr2.setExcentricity(0.8);
		attr2.setHoraryRotation(false);
		attr2.setInclination(30);
		attr2.setMass(50000);
		attr2.setName("Spot");
		attr2.setOrbitalPeriod(31536000);
		attr2.setOrbitMoment(1);
		attr2.setOrbitRadius(400000000);
		attr2.setOrder(2);
		attr2.setRadius(8000000);
		attr2.setResourceAlias("Spot");
		attr2.setSideralPeriod(6000000);
		attr2.setVolume(60000000);
		
		RealAttributes attr3 = new RealAttributes();
		//attr1.setOrder(1);
		attr3.setAphelion(2000000);
		attr3.setCenter("Spot");
		attr3.setCorpseType(CorpseType.PLANET);
		attr3.setDensity(40000);
		attr3.setExcentricity(0.5);
		attr3.setHoraryRotation(false);
		attr3.setInclination(150);
		attr3.setMass(50000);
		attr3.setName("Serendip");
		attr3.setOrbitalPeriod(8536000);
		attr3.setOrbitMoment(1);
		attr3.setOrbitRadius(50000000);
		attr3.setOrder(3);
		attr3.setRadius(2000000);
		attr3.setResourceAlias("Serendip");
		attr3.setSideralPeriod(6000000);
		attr3.setVolume(60000000);
		
		RealAttributes attr4 = new RealAttributes();
		//attr1.setOrder(1);
		attr4.setAphelion(2000000);
		attr4.setCenter("Serendip");
		attr4.setCorpseType(CorpseType.PLANET);
		attr4.setDensity(40000);
		attr4.setExcentricity(1);
		attr4.setHoraryRotation(true);
		attr4.setInclination(20);
		attr4.setMass(50000);
		attr4.setName("Tank");
		attr4.setOrbitalPeriod(2536000);
		attr4.setOrbitMoment(1);
		attr4.setOrbitRadius(40000000);
		attr4.setOrder(4);
		attr4.setRadius(1500000);
		attr4.setResourceAlias("Tank");
		attr4.setSideralPeriod(6000000);
		attr4.setVolume(60000000);
		
		RealAttributes attr5 = new RealAttributes();
		//attr1.setOrder(1);
		attr5.setAphelion(2000000);
		attr5.setCenter("Tank");
		attr5.setCorpseType(CorpseType.PLANET);
		attr5.setDensity(40000);
		attr5.setExcentricity(0.9);
		attr5.setHoraryRotation(false);
		attr5.setInclination(180);
		attr5.setMass(50000);
		attr5.setName("Ako");
		attr5.setOrbitalPeriod(15536000);
		attr5.setOrbitMoment(1);
		attr5.setOrbitRadius(5000000);
		attr5.setOrder(5);
		attr5.setRadius(1000000);
		attr5.setResourceAlias("Ako");
		attr5.setSideralPeriod(6000000);
		attr5.setVolume(60000000);
		
		RealAttributes attr6 = new RealAttributes();
		//attr1.setOrder(1);
		attr6.setAphelion(2000000);
		attr6.setCenter("Ako");
		attr6.setCorpseType(CorpseType.PLANET);
		attr6.setDensity(40000);
		attr6.setExcentricity(0.3);
		attr6.setHoraryRotation(false);
		attr6.setInclination(60);
		attr6.setMass(50000);
		attr6.setName("Charon");
		attr6.setOrbitalPeriod(5536000);
		attr6.setOrbitMoment(1);
		attr6.setOrbitRadius(10000000);
		attr6.setOrder(6);
		attr6.setRadius(1500000);
		attr6.setResourceAlias("Charon");
		attr6.setSideralPeriod(6000000);
		attr6.setVolume(60000000);
		
		RealAttributes attr7 = new RealAttributes();
		//attr1.setOrder(1);
		attr7.setAphelion(2000000);
		attr7.setCenter("Charon");
		attr7.setCorpseType(CorpseType.PLANET);
		attr7.setDensity(40000);
		attr7.setExcentricity(0.7);
		attr7.setHoraryRotation(false);
		attr7.setInclination(20);
		attr7.setMass(50000);
		attr7.setName("Titan");
		attr7.setOrbitalPeriod(3536000);
		attr7.setOrbitMoment(1);
		attr7.setOrbitRadius(7000000);
		attr7.setOrder(7);
		attr7.setRadius(1000000);
		attr7.setResourceAlias("Titan");
		attr7.setSideralPeriod(6000000);
		attr7.setVolume(60000000);
		
		SolarSystemData solarData = new SolarSystemData();
		solarData.addCorpse(attr1);
		solarData.addCorpse(attr2);
		solarData.addCorpse(attr3);
		solarData.addCorpse(attr4);
		solarData.addCorpse(attr5);
		solarData.addCorpse(attr6);
		solarData.addCorpse(attr7);
		return solarData;
	}
}
